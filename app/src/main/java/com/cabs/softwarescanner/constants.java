package com.cabs.softwarescanner;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

public class constants {

    public static final String CUSTOMER_URL = "http://130.211.252.62:8069/api/route";
    public static final String COLLECTION_URL = "http://130.211.252.62:8069/api/collectionex";
    public static final String LOGIN_URL = "http://130.211.252.62:8069/web/session/authenticate";

    public static final int PLAY_SERVICES_RESOLUTION_REQUEST = 1000;

    public static final String BARCODE_LICENSE = "9508C0CC0A505B0EF624ABF35336FEE3";
    public static final String MINT_API_KEY = "9996a473";

    public static final String settings_filename = "settings";
    public static final String session_expiry = "session_expiry";
    public static final String session_id = "session_id";
    public static final String lastloginday = "lastloginday";
    public static final String lastsyncday = "lastsyncday";
    public static final String route_id = "route_id";
    public static final String vehicle_id = "vehicle_id";
    public static final String visited_list = "visited_list";

    public static final String username = "username";
    public static final String password = "password";
    public static final String database = "database";
    public static final String barcode_size = "barcode_size";
    public static final String max_weight = "max_weight";
    public static final String customer_Id_check = "customer_Id_check";
    public static final String print_manifest = "print_manifest";
    public static final String sms_Manifest = "sms_Manifest";

    public static final String container_check = "container_check";
    public static final String sharps_check = "sharps_check";
    public static final String others_check = "others_check";
    public static final String logo_check = "logo_check";

    public static final String TAG = "SPP_TERMINAL";
    public static final int DataStorageDuration = 2;

    public static final int negativecheck = 1;
    public static final int maxweightcheck = 2;

    public static final String CodeLand = "CodeLand";
    public static final String Medicare = "Medicare";
    public static final String Maridi = "Maridi";
    public static final String AECS = "AECS";


    public static String GetToday(){
        Date presentTime = Calendar.getInstance().getTime();
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        dateFormat.setTimeZone(TimeZone.getTimeZone("GMT"));
        return dateFormat.format(presentTime);
    }
}