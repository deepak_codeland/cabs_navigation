package com.cabs.softwarescanner;

import android.database.Cursor;

import com.cabs.softwarescanner.database.customerdb;

import java.io.Serializable;

public class customer implements Serializable {

    public String mName;
    public String mData;
    public int mId;
    public double mLatitude = 0.0;
    public double mLongitude = 0.0;
    public String mVisitTime;
    public int mVisitDay;
    public String mVisitTimeGMT = "";
    public int cID;
    public String mMobileData;

    public transient customerdb mdb;

    public customer(customerdb db) {
        mdb = db;
    }

    public customer(int Id, String Name) {
        mName = Name;
        mId = Id;
    }

    public customer() {
    }

    public boolean setVisited(String visitTime, int visitDay) {
        if (mdb == null){
            return false;
        }

        Cursor c = mdb.getCustomerMobileData(mId);
        String mobileData = "";
        if (c.moveToNext()) {
            try {
                mobileData = c.getString(2);
            }catch (Exception e){
                e.printStackTrace();
            }
        }


        if (mdb.isCustomerVisited(mId)){
            if (mdb.putCustomer(mId,mName,mData,true,false, visitTime, visitDay, mobileData) >0){
                return true;
            }
        }else {
            if( mdb.updateCustomer(mId, mData, true, false,visitTime, visitDay) > 0){
                return true;
            }else if (mdb.putCustomer(mId,mName,mData,true,false, visitTime, visitDay, mobileData) >0){
                return true;
            }
        }
        return false;
    }

    public boolean load(long id) {
        if (mdb != null) {
            mData = mdb.getCustomer(id);
        }
        return !mData.isEmpty();
    }
}
