package com.cabs.softwarescanner.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.provider.BaseColumns;

public class customerdb extends SQLiteOpenHelper {

    // If you change the database schema, you must increment the database version.
    public static final int DATABASE_VERSION = 4;
    public static final String DATABASE_NAME;
    private static final String TEXT_TYPE;
    private static final String INTEGER_TYPE;
    private static final String COMMA_SEP;

    private static final String SQL_CREATE_ENTRIES;
    private static final String SQL_DELETE_ENTRIES;
    private static customerdb mInstance = null;

    static {
        DATABASE_NAME = "customer.db";
        TEXT_TYPE = " TEXT";
        INTEGER_TYPE = " INTEGER";
        COMMA_SEP = ",";
        SQL_CREATE_ENTRIES = "CREATE TABLE " + customerentry.TABLE_NAME + " (" +
                customerentry._ID + " INTEGER PRIMARY KEY," +
                customerentry.COLUMN_CUSTOMER_ID + TEXT_TYPE + COMMA_SEP +
                customerentry.COLUMN_CUSTOMER_NAME + TEXT_TYPE + COMMA_SEP +
                customerentry.COLUMN_CUSTOMER_VISITED + INTEGER_TYPE + COMMA_SEP +
                customerentry.COLUMN_CUSTOMER_SYNCED + INTEGER_TYPE + COMMA_SEP +
                customerentry.COLUMN_CUSTOMER_DATA + TEXT_TYPE + COMMA_SEP +
                customerentry.COLUMN_CUSTOMER_VISITTIME + TEXT_TYPE + COMMA_SEP +
                customerentry.COLUMN_CUSTOMER_VISITDAY + INTEGER_TYPE + COMMA_SEP +
                customerentry.COLUMN_CUSTOMER_MOBILE_DATA + TEXT_TYPE +" )";
        SQL_DELETE_ENTRIES = "DROP TABLE IF EXISTS " + customerentry.TABLE_NAME;
    }

    /* Inner class that defines the table contents */
    public static abstract class customerentry implements BaseColumns {
        public static final String TABLE_NAME = "customer";
        public static final String COLUMN_CUSTOMER_ID = "id";
        public static final String COLUMN_CUSTOMER_NAME = "name";
        public static final String COLUMN_CUSTOMER_VISITED = "visited";
        public static final String COLUMN_CUSTOMER_SYNCED = "synced";
        public static final String COLUMN_CUSTOMER_DATA = "data";
        public static final String COLUMN_CUSTOMER_VISITTIME = "visit_time";
        public static final String COLUMN_CUSTOMER_VISITDAY = "visit_day";
        public static final String COLUMN_CUSTOMER_MOBILE_DATA = "mobile_data";
    }


    public static customerdb getInstance(Context ctx) {

        if (mInstance == null) {
            mInstance = new customerdb(ctx.getApplicationContext());
        }
        return mInstance;
    }

    public customerdb(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    public void onCreate(SQLiteDatabase db) {
        db.execSQL(SQL_CREATE_ENTRIES);
    }

    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // This database is only a cache for online data, so its upgrade policy is
        // to simply to discard the data and start over
        db.execSQL(SQL_DELETE_ENTRIES);
        onCreate(db);
    }

    public void onDowngrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        onUpgrade(db, oldVersion, newVersion);
    }

    public String getCustomer(long customerid) {

        SQLiteDatabase db = getReadableDatabase();

        // Define a projection that specifies which columns from the database
        // you will actually use after this query.
        String[] projection = {customerentry._ID, customerentry.COLUMN_CUSTOMER_NAME,customerentry.COLUMN_CUSTOMER_DATA};

        // How you want the results sorted in the resulting Cursor
        String sortOrder = customerentry._ID + " DESC";

        Cursor c = db.query(customerentry.TABLE_NAME,  // The table to query
                projection, customerentry.COLUMN_CUSTOMER_ID + "=?", new String[]{Long.toString(customerid)},
                null, null, sortOrder);

        if (c.moveToFirst())
            return c.getString(c.getColumnIndexOrThrow(customerentry.COLUMN_CUSTOMER_DATA));

        return "";
    }

    public Cursor getCustomers(boolean visited) {

        SQLiteDatabase db = getReadableDatabase();

        // Define a projection that specifies which columns from the database
        // you will actually use after this query.
        String[] projection = {customerentry._ID, customerentry.COLUMN_CUSTOMER_NAME, customerentry.COLUMN_CUSTOMER_DATA, customerentry.COLUMN_CUSTOMER_VISITTIME, customerentry.COLUMN_CUSTOMER_ID,customerentry.COLUMN_CUSTOMER_MOBILE_DATA};

        // How you want the results sorted in the resulting Cursor
        if (visited){
            String sortOrder = customerentry.COLUMN_CUSTOMER_VISITTIME + " DESC";
            return db.query(customerentry.TABLE_NAME,
                    projection, customerentry.COLUMN_CUSTOMER_VISITED + "=1", null,
                    null, null, sortOrder);
        }
        else {
            String sortOrder = customerentry._ID + " DESC";
            return db.query(customerentry.TABLE_NAME,
                    projection, customerentry.COLUMN_CUSTOMER_VISITED + "=0", null,
                    null, null, sortOrder);
        }
    }

    public Cursor getCustomerstoSync() {

        SQLiteDatabase db = getReadableDatabase();

        String[] projection = {customerentry._ID, customerentry.COLUMN_CUSTOMER_ID,customerentry.COLUMN_CUSTOMER_DATA, customerentry.COLUMN_CUSTOMER_NAME};
        String sortOrder = customerentry._ID + " DESC";

        return db.query(customerentry.TABLE_NAME,
                projection, customerentry.COLUMN_CUSTOMER_SYNCED + "=0 and " +
                customerentry.COLUMN_CUSTOMER_VISITED + "=1" , null,
                null, null, sortOrder);
    }

    public Cursor getCustomersAlreadySynced() {

        SQLiteDatabase db = getReadableDatabase();

        String[] projection = {customerentry._ID, customerentry.COLUMN_CUSTOMER_ID,customerentry.COLUMN_CUSTOMER_DATA, customerentry.COLUMN_CUSTOMER_NAME};
        String sortOrder = customerentry._ID + " DESC";

        return db.query(customerentry.TABLE_NAME,
                projection, customerentry.COLUMN_CUSTOMER_SYNCED + "=1 and " +
                        customerentry.COLUMN_CUSTOMER_VISITED + "=1" , null,
                null, null, sortOrder);
    }

    public long putCustomer(int customerid, String name,String data, boolean visited, boolean synced, String visitTime, int visitDay, String mobileData) {
        // Gets the data repository in write mode
        SQLiteDatabase db = getWritableDatabase();

        // Create a new map of values, where column names are the keys
        ContentValues values = new ContentValues();
        values.put(customerentry.COLUMN_CUSTOMER_ID, customerid);
        values.put(customerentry.COLUMN_CUSTOMER_NAME, name);
        values.put(customerentry.COLUMN_CUSTOMER_DATA, data);
        values.put(customerentry.COLUMN_CUSTOMER_VISITED, visited);
        values.put(customerentry.COLUMN_CUSTOMER_SYNCED, synced);
        values.put(customerentry.COLUMN_CUSTOMER_VISITTIME, visitTime);
        values.put(customerentry.COLUMN_CUSTOMER_VISITDAY, visitDay);
        values.put(customerentry.COLUMN_CUSTOMER_MOBILE_DATA, mobileData);

        // Insert the new row, returning the primary key value of the new row
        return db.insert(customerentry.TABLE_NAME, null, values);
    }

    public long updateCustomer(int customerid, String data, boolean visited, boolean synced, String visitTime, int visitDay) {
        SQLiteDatabase db = getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(customerentry.COLUMN_CUSTOMER_DATA, data);
        values.put(customerentry.COLUMN_CUSTOMER_VISITED, visited);
        values.put(customerentry.COLUMN_CUSTOMER_SYNCED, synced);
        values.put(customerentry.COLUMN_CUSTOMER_VISITTIME, visitTime);
        values.put(customerentry.COLUMN_CUSTOMER_VISITDAY, visitDay);

        String selection = customerentry.COLUMN_CUSTOMER_ID + " LIKE ?";
        String[] selectionArgs = {String.valueOf(customerid)};
        return db.update(customerentry.TABLE_NAME, values, selection, selectionArgs);
    }

    public long setSynced(int customerid, boolean visited, boolean synced) {
        SQLiteDatabase db = getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(customerentry.COLUMN_CUSTOMER_VISITED, visited);
        values.put(customerentry.COLUMN_CUSTOMER_SYNCED, synced);

//        String selection = customerentry.COLUMN_CUSTOMER_ID + " LIKE ?";
        String selection = customerentry._ID + " LIKE ?";
        String[] selectionArgs = {String.valueOf(customerid)};
        return db.update(customerentry.TABLE_NAME, values, selection, selectionArgs);
    }

    public boolean isCustomerVisited(int customerId){
        SQLiteDatabase db = getReadableDatabase();

        String[] projection = {customerentry._ID, customerentry.COLUMN_CUSTOMER_ID,customerentry.COLUMN_CUSTOMER_VISITTIME};
        String sortOrder = customerentry._ID + " DESC";
        String selection = customerentry.COLUMN_CUSTOMER_ID + " LIKE ?";
        String[] selectionArgs = {String.valueOf(customerId)};
        Cursor c = db.query(customerentry.TABLE_NAME, projection, selection , selectionArgs,
                null, null, sortOrder);

        if (c == null){
            return false;
        }else if (c.moveToNext()){
            String visit_time = c.getString(2);
            if (visit_time == null){
                return false;
            }else {
                return true;
            }
        }else {
          return false;
        }
    }

    public Cursor getCustomerMobileData(int customerId){

        SQLiteDatabase db = getReadableDatabase();

        String[] projection = {customerentry._ID, customerentry.COLUMN_CUSTOMER_ID,customerentry.COLUMN_CUSTOMER_MOBILE_DATA};
        String sortOrder = customerentry._ID + " DESC";
        String selection = customerentry.COLUMN_CUSTOMER_ID + " LIKE ?";
        String[] selectionArgs = {String.valueOf(customerId)};
        Cursor c = db.query(customerentry.TABLE_NAME, projection, selection , selectionArgs,
                null, null, sortOrder);

        return c;
    }

    public long removeSyncedCustomer() {
        SQLiteDatabase db = getWritableDatabase();
        return db.delete(customerentry.TABLE_NAME,  customerentry.COLUMN_CUSTOMER_SYNCED + "=1" , null);
    }

    public long removeNonVisitedCustomer() {
        SQLiteDatabase db = getWritableDatabase();

        String selection = customerentry.COLUMN_CUSTOMER_VISITED + " =0";
        String[] selectionArgs = {String.valueOf(false)};

        return db.delete(customerentry.TABLE_NAME, selection, null);
    }

    public void clear() {
        removeSyncedCustomer();
    }

    public long removeOldData(int lastestDay){
        if (lastestDay < 0){
            lastestDay = lastestDay + 365;
        }
        SQLiteDatabase db = getWritableDatabase();
        db.beginTransaction();
        long l;
        try {
            String selection = customerentry.COLUMN_CUSTOMER_VISITDAY + " < " + lastestDay;
            l = db.delete(customerentry.TABLE_NAME, selection, null);
            db.setTransactionSuccessful();
        } finally {
            db.endTransaction();
        }

        db.close();
        return l;
    }
}
