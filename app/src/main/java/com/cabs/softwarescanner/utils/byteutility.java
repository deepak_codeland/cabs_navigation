package com.cabs.softwarescanner.utils;

import android.text.InputFilter;
import android.text.Spanned;
import android.util.Log;

/**
 * Created by lin on 06/10/2016.
 */
public class byteutility {

    public static String printHex(String hex) {
        StringBuilder sb = new StringBuilder();
        int len = hex.length();
        try {
            for (int i = 0; i < len; i += 2) {
                sb.append("0x").append(hex.substring(i, i + 2)).append(" ");
            }
        } catch (NumberFormatException e) {
            Log.e("printHex", e.getMessage());
        } catch (StringIndexOutOfBoundsException e) {
            Log.e("printHex", e.getMessage());
        }
        return sb.toString();
    }

    public static byte[] toHex(String hex) {
        int len = hex.length();
        byte[] result = new byte[len];
        try {
            int index = 0;
            for (int i = 0; i < len; i += 2) {
                result[index] = (byte) Integer.parseInt(hex.substring(i, i + 2), 16);
                index++;
            }
        } catch (NumberFormatException e) {
            Log.e("toHex" , e.getMessage());
        } catch (StringIndexOutOfBoundsException e) {
            Log.e("toHex" , e.getMessage());
        }
        return result;
    }

    public static byte[] concat(byte[] A, byte[] B) {
        byte[] C = new byte[A.length + B.length];
        System.arraycopy(A, 0, C, 0, A.length);
        System.arraycopy(B, 0, C, A.length, B.length);
        return C;
    }

    public static int mod(int x, int y) {
        int result = x % y;
        return result < 0 ? result + y : result;
    }

    public static class InputFilterHex implements InputFilter {

        @Override
        public CharSequence filter(CharSequence source, int start, int end, Spanned dest, int dstart, int dend) {
            for (int i = start; i < end; i++) {
                if (!Character.isDigit(source.charAt(i))
                        && source.charAt(i) != 'A' && source.charAt(i) != 'D'
                        && source.charAt(i) != 'B' && source.charAt(i) != 'E'
                        && source.charAt(i) != 'C' && source.charAt(i) != 'F'
                        ) {
                    return "";
                }
            }
            return null;
        }
    }
}
