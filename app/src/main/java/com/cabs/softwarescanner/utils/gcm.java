package com.cabs.softwarescanner.utils;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.Toast;
import com.google.android.gms.gcm.GoogleCloudMessaging;
import org.json.JSONObject;
import java.io.IOException;

import com.cabs.softwarescanner.interfaces.IAsyncResponse;

public class gcm implements IAsyncResponse{

    public boolean initialiseGCM(Context appContext, Activity activity) {
        mActivity = activity;
        context = appContext;
        try {
            SENDER_ID = "306438931682";
            regId = getRegistrationId(context);

            if (regId.isEmpty()) {
                registerInBackground();
            }
        } catch (Exception e) {
            Log.e(mTag, "GCM registration failed");
            return false;
        }
        return true;
    }

    private  String getRegistrationId(Context context) {

        SharedPreferences settings = mActivity.getSharedPreferences("settings", 0);
        String registrationId = settings.getString("gcmkey", "");
        if (registrationId.isEmpty()) {
            Log.i(mTag, "Registration not found.");
            return "";
        }

        int registeredVersion = settings.getInt("version", 0);
        int currentVersion = getAppVersion(context);

        if(registeredVersion == 0 ) {
            SharedPreferences.Editor editor = settings.edit();
            editor.putInt("version", currentVersion);
        } else if (registeredVersion != currentVersion) {
            Log.i(mTag, "App version changed.");
            return "";
        }

        return registrationId;
    }

    /**
     * @return Application's version code from the {@code PackageManager}.
     */
    private  int getAppVersion(Context context) {

        try {
            PackageInfo packageInfo = context.getPackageManager()
                    .getPackageInfo(context.getPackageName(), 0);
            return packageInfo.versionCode;
        } catch (PackageManager.NameNotFoundException e) {
            // should never happen
            throw new RuntimeException("Could not get package name: " + e);
        }
    }

    /**
     * Registers the application with GCM servers asynchronously.
     * Stores the registration ID and the app versionCode in the application's
     * shared preferences.
     */
    private void registerInBackground() {

        registrationDialog = ProgressDialog.show(mActivity, "", "Please wait while we register you for notifications...");
        new AsyncTask<Void, Void, String>() {

            @Override
            protected String doInBackground(Void... params) {

                String msg = "";
                try {
                    if (gcm == null) {
                        gcm = GoogleCloudMessaging.getInstance(context);
                    }
                    regId = gcm.register(SENDER_ID);
                } catch (IOException ex) {
                    msg = "Error :" + ex.getMessage();
                }
                return msg;
            }

            @Override
            protected void onPostExecute(String msg) {
                storeRegistrationId(context, regId);
                //sendRegistrationIdToBackend(regId);
            }
        }.execute(null, null, null);
    }

    /**
     * Stores the registration ID and the app versionCode in the application's
     * {@code SharedPreferences}.
     *
     * @param context application's context.
     * @param regId   registration ID
     */
    private void storeRegistrationId(Context context, String regId) {

        SharedPreferences settings = mActivity.getSharedPreferences("settings", 0);
        SharedPreferences.Editor editor = settings.edit();
        editor.putString("gcmkey", regId);

        // Commit the edits!
        editor.commit();
    }

    @Override
    public void processFinish(JSONObject response) {
        try{

            if(response.getBoolean("success")) {
                Toast.makeText(mActivity, "GCM registered successfully...", Toast.LENGTH_LONG).show();
                return;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            Toast.makeText(mActivity, "GCM registration failed. Please contact admin.", Toast.LENGTH_LONG).show();
        }
    }

    private  GoogleCloudMessaging gcm;

    private String regId;
    private Context context;
    private String SENDER_ID;
    private final String mTag = "GCM";
    private ProgressDialog registrationDialog = null;
    private Activity mActivity;
}