package com.cabs.softwarescanner.utils;

import android.app.Application;

import com.cabs.softwarescanner.constants;
import com.splunk.mint.Mint;

/**
 * Created by DEEPAK on 5/24/2017.
 */

public class CrashReporting extends Application {

    @Override
    public void onCreate() {
        super.onCreate();

        Mint.initAndStartSession(this, constants.MINT_API_KEY);
        Mint.enableLogging(true);

        //Mint.setLogging("*:W");
    }
}
