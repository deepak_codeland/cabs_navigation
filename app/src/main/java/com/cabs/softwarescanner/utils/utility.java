package com.cabs.softwarescanner.utils;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Build;
import android.preference.PreferenceManager;
import android.provider.Settings;
import android.util.Log;
import android.widget.Toast;

import com.cabs.softwarescanner.R;
import com.cabs.softwarescanner.constants;
import com.cabs.softwarescanner.interfaces.IAsyncResponse;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;

import org.json.JSONObject;

import java.util.Calendar;
import java.util.GregorianCalendar;

/**
 * Created by lin on 26/08/2016.
 */
public class utility {

    public static boolean IsLoggedInToday(SharedPreferences pref) {

        SharedPreferences.Editor editor = pref.edit();
        int day = pref.getInt(constants.lastloginday, 0);
        int today = Calendar.getInstance().get(Calendar.DAY_OF_YEAR);

        return (today == day);
    }


    public static void resetLastLoginDay(SharedPreferences pref) {
        SharedPreferences.Editor editor = pref.edit();
        editor.putInt(constants.lastloginday, 0);
        editor.apply();
    }

    public static boolean IsSessionActive(SharedPreferences pref) {

        long time = pref.getLong(constants.session_expiry, 0);
        if(new GregorianCalendar().getTimeInMillis() < time) {
            Log.v("Utils", "Session is still active");
            return true;
        }

        return false;
    }

    public static boolean IsSettingsSet(SharedPreferences sharedPrefs){
        String database = sharedPrefs.getString(constants.database,"0");
        return !database.equals("0");
    }

    public static boolean checkPlayServices(Context context, Activity activity) {
        int resultCode = GooglePlayServicesUtil.isGooglePlayServicesAvailable(context);
        if (resultCode != ConnectionResult.SUCCESS) {
            if (GooglePlayServicesUtil.isUserRecoverableError(resultCode)) {
                GooglePlayServicesUtil.getErrorDialog(resultCode, activity, constants.PLAY_SERVICES_RESOLUTION_REQUEST).show();
            }
            else {
                Toast.makeText(context, context.getString(R.string.googleplay_error_msg), Toast.LENGTH_LONG).show();
            }
            return false;
        }
        return true;
    }

    public static void Login( Context context,SharedPreferences pref, IAsyncResponse responseHandler){
        try {

            /* Unit testing...*/
            JSONObject jsonObject = new JSONObject();
            JSONObject json = new JSONObject();
            jsonObject.put("login", pref.getString(constants.username, "admin-test"));
            jsonObject.put("password", pref.getString(constants.password,"admin-test"));
            jsonObject.put("db", pref.getString(constants.database,"tamilnadu"));
            json.put("params", jsonObject);
            /* Unit testing...*/

            communicator.LoginRequest login = new communicator.LoginRequest(constants.LOGIN_URL,json, responseHandler);
            login.execute();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static boolean saveSession(SharedPreferences pref, String session, long sessionexpiry){

        if( session.isEmpty())
            return false;

        SharedPreferences.Editor editor = pref.edit();
        editor.putLong(constants.session_expiry, sessionexpiry);
        editor.putString(constants.session_id, session);

        int today = Calendar.getInstance().get(Calendar.DAY_OF_YEAR);
        editor.putInt(constants.lastloginday, today);

        editor.apply();
        return true;
    }

    public static void showWeightAlert(int from, Context context, android.app.AlertDialog weightAlertDialog, android.app.AlertDialog barcodeAlertDialog) {

        if (!weightAlertDialog.isShowing() && (barcodeAlertDialog == null || !barcodeAlertDialog.isShowing()) ) {

            switch(from){
                case constants.negativecheck:
                    weightAlertDialog.setTitle("Negative weight from weighing scale!");
                    weightAlertDialog.setMessage("Reset the scale to 0.0...");
                    break;
                case constants.maxweightcheck:
                    SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(context);
                    weightAlertDialog.setTitle("Weight for the bag is greater than " + Double.parseDouble(pref.getString(constants.max_weight, "35")) + " KG");
                    weightAlertDialog.setMessage("Check weight of the bag...");
                    break;
            }

            weightAlertDialog.show();
        }
    }



    public static boolean isTimeAutomatic(Context c) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
            return Settings.Global.getInt(c.getContentResolver(), Settings.Global.AUTO_TIME_ZONE, 0) == 1
                    && Settings.Global.getInt(c.getContentResolver(), Settings.Global.AUTO_TIME, 0) == 1;

        } else {
            return android.provider.Settings.System.getInt(c.getContentResolver(), android.provider.Settings.System.AUTO_TIME, 0) == 1
                    && android.provider.Settings.System.getInt(c.getContentResolver(), Settings.System.AUTO_TIME_ZONE, 0) == 1;
        }
    }

    public static String getPrefence(Context context, String item) {
        final SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(context);
        return settings.getString(item, constants.TAG);
    }
    // ============================================================================


    /**
     * Получение флага из настроек
     */
    public static boolean getBooleanPrefence(Context context, String tag) {
        final SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(context);
        return settings.getBoolean(tag, true);
    }
}
