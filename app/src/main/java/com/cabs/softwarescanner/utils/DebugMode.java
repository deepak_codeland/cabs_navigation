package com.cabs.softwarescanner.utils;

import android.app.Application;

public class DebugMode extends Application{

    private static boolean debugMode;

    public static boolean isDebugMode() {
        return debugMode;
    }

    public static boolean setDebugMode(boolean mode) {
        debugMode = mode;
        return debugMode;
    }
}
