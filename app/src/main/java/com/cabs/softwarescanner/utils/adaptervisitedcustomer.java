package com.cabs.softwarescanner.utils;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.Filter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.cabs.softwarescanner.BagDetails;
import com.cabs.softwarescanner.R;
import com.cabs.softwarescanner.constants;
import com.cabs.softwarescanner.customer;
import com.cabs.softwarescanner.ui.activityvisited;

import org.json.JSONObject;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.TimeZone;

public class adaptervisitedcustomer extends BaseExpandableListAdapter{

    private Activity activity;
    private ArrayList<customer> mOriginalValues;
    private ArrayList<customer> mDisplayedValues;
    private static LayoutInflater inflater = null;

    private ArrayList<customer> _listDataHeaderVisitedCustomer; // header titles
    // child data in format of header title, child title
    private HashMap<String, ArrayList<BagDetails>> _listDataChildBagDetails;

    public adaptervisitedcustomer(Activity activity, int textViewResourceId, ArrayList<customer> _lCustomer,
                                  HashMap<String, ArrayList<BagDetails>> listChildData) {

        try {
            this.activity = activity;
            this._listDataHeaderVisitedCustomer = _lCustomer;
            this.mOriginalValues = _lCustomer;
            this.mDisplayedValues = _lCustomer;
            this._listDataChildBagDetails = listChildData;
            inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        } catch (Exception e) {
            e.printStackTrace();
        }


    }

    @Override
    public int getGroupCount() {
        return _listDataHeaderVisitedCustomer.size();
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        return 1;
    }

    @Override
    public Object getGroup(int groupPosition) {
        return this._listDataHeaderVisitedCustomer.get(groupPosition);
    }

    @Override
    public Object getChild(int groupPosition, int childPosition) {
        return this._listDataChildBagDetails.get(this._listDataHeaderVisitedCustomer.get(groupPosition).mName);
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    private static class ViewHolder {
        TextView display_name;
        TextView total_count;
        TextView total_weight;
    }

    public android.widget.Filter getFilter(){
        Filter filter = new Filter() {

            @SuppressWarnings("unchecked")
            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {
                mDisplayedValues = (ArrayList<customer>) results.values; // has the filtered values
                notifyDataSetChanged();  // notifies the data with new filtered values
            }

            @Override
            protected FilterResults performFiltering(CharSequence constraint) {
                FilterResults results = new FilterResults();        // Holds the results of a filtering operation in values
                ArrayList<customer> FilteredArrList = new ArrayList<>();

                if (mOriginalValues == null) {
                    mOriginalValues = new ArrayList<>(mDisplayedValues); // saves the original data in mOriginalValues
                }

                // If constraint(CharSequence that is received) is null returns the mOriginalValues(Original) values
                if (constraint == null || constraint.length() == 0) {
                    results.count = mOriginalValues.size();
                    results.values = mOriginalValues;
                } else {
                    constraint = constraint.toString().toLowerCase();
                    for(int i = 0; i < mOriginalValues.size(); i++) {
                        String data = mOriginalValues.get(i).mName;
                        if (data.toLowerCase().startsWith(constraint.toString())) {
                            FilteredArrList.add(new customer(mOriginalValues.get(i).mId,mOriginalValues.get(i).mName));
                        }
                    }
                    // set the Filtered result to return
                    results.count = FilteredArrList.size();
                    results.values = FilteredArrList;
                }
                return results;
            }
        };
        return filter;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {
        View vi = convertView;
        final ViewHolder holder;
        try {
            if (convertView == null) {
                vi = inflater.inflate(R.layout.visited_list_item, null);
                holder = new ViewHolder();

                holder.display_name = (TextView) vi.findViewById(R.id.visited_customer_name);
                holder.total_count = (TextView) vi.findViewById(R.id.total_count);
                holder.total_weight = (TextView) vi.findViewById(R.id.total_weight);

                vi.setTag(holder);
            } else {
                holder = (ViewHolder) vi.getTag();
            }

            customer custObj = (customer) getGroup(groupPosition);

            String visit_Time = custObj.mVisitTime;

            DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            formatter.setTimeZone(TimeZone.getTimeZone("GMT"));
            Date visit_Time_date = formatter.parse(visit_Time);
            SimpleDateFormat newFormat = new SimpleDateFormat("dd-MM-yyyy  hh:mm a");
            newFormat.setTimeZone(TimeZone.getTimeZone("Asia/Calcutta"));
            String finalVisitTimeDate = newFormat.format(visit_Time_date);

            holder.display_name.setText(custObj.mName+ ":\n"+ finalVisitTimeDate);

            JSONObject jsonObject = new JSONObject(custObj.mData);
            jsonObject.getJSONObject("params").getInt("total_count");
            holder.total_count.setText( String.valueOf(jsonObject.getJSONObject("params").getInt("total_count") + " Nos"));
            double val = jsonObject.getJSONObject("params").getDouble("total_weight");
            //double val = weightTotalOnVisited;
            val = val*100;
            val = Math.round(val);
            val = val /100;
            holder.total_weight.setText(String.valueOf(val + " Kgs"));

            custObj.mVisitTimeGMT = finalVisitTimeDate;

        } catch (Exception e) {
            e.printStackTrace();
        }

        return vi;
    }

    @Override
    public View getChildView(int groupPosition, int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {
        customer customer = _listDataHeaderVisitedCustomer.get(groupPosition);
        String customerName = customer.mName+ ":\n"+ customer.mVisitTime;
        ArrayList<BagDetails> bagDetailsArrayList = _listDataChildBagDetails.get(customerName);
        BagDetails childBagDetails = null; //bagDetailsArrayList.get(0);
        if(!bagDetailsArrayList.isEmpty()) {
            childBagDetails = bagDetailsArrayList.get(0);
        }

        SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(activity);
        boolean isContainerChecked = Boolean.parseBoolean(String.valueOf(pref.getBoolean(constants.container_check, false)));
        boolean isSharpsChecked = Boolean.parseBoolean(String.valueOf(pref.getBoolean(constants.sharps_check, false)));
        boolean isOthersChecked = Boolean.parseBoolean(String.valueOf(pref.getBoolean(constants.others_check, false)));

        //Declaring Visible Variables
        TextView textView1, textView2, textView3, textView4, textView5, textView6, textView7; //for count
        TextView textView11, textView22, textView33, textView44, textView55, textView66, textView77; // for weight
        ImageButton manifestButton;
        ImageButton smsButton;

        View vi = convertView;
        final ViewHolder holder;
        try {
            if (convertView == null) {
                vi = inflater.inflate(R.layout.bagdetails, null);
            }
//            else {
//                //holder = (ViewHolder) vi.getTag();
//            }
            if ( !isContainerChecked){

                LinearLayout linearLayoutContainer = (LinearLayout) vi.findViewById(R.id.linearLayoutContainers);
                ImageView imageViewContainer = (ImageView) vi.findViewById(R.id.imageViewContainers);
                linearLayoutContainer.setVisibility(View.GONE);
                imageViewContainer.setVisibility(View.GONE);
            }

            if ( !isSharpsChecked){

                LinearLayout linearLayoutSharps = (LinearLayout) vi.findViewById(R.id.linearLayoutSharps);
                ImageView imageViewSharps = (ImageView) vi.findViewById(R.id.imageViewSharps);
                linearLayoutSharps.setVisibility(View.GONE);
                imageViewSharps.setVisibility(View.GONE);
            }

            if ( !isOthersChecked){

                LinearLayout linearLayoutOthers = (LinearLayout) vi.findViewById(R.id.linearLayoutOthers);
                ImageView imageViewOthers = (ImageView) vi.findViewById(R.id.imageViewOthers);
                linearLayoutOthers.setVisibility(View.GONE);
                imageViewOthers.setVisibility(View.GONE);
            }

            //Initializing variables
            textView1 = (TextView) vi.findViewById(R.id.blue_count);
            textView2 = (TextView) vi.findViewById(R.id.red_count);
            textView3 = (TextView) vi.findViewById(R.id.yellow_count);
            textView4 = (TextView) vi.findViewById(R.id.container_count);
            textView5 = (TextView) vi.findViewById(R.id.sharps_count);
            textView6 = (TextView) vi.findViewById(R.id.other_count);
            textView7 = (TextView) vi.findViewById(R.id.whites_count);

            textView11 = (TextView) vi.findViewById(R.id.blue_weight);
            textView22 = (TextView) vi.findViewById(R.id.red_weight);
            textView33 = (TextView) vi.findViewById(R.id.yellow_weight);
            textView44 = (TextView) vi.findViewById(R.id.container_weight);
            textView55 = (TextView) vi.findViewById(R.id.sharps_weight);
            textView66 = (TextView) vi.findViewById(R.id.other_weight);
            textView77 = (TextView) vi.findViewById(R.id.whites_weight);

            manifestButton = (ImageButton) vi.findViewById(R.id.sendManifest);
            smsButton = (ImageButton) vi.findViewById(R.id.sendSmsManifest);

            //GETTING BAG DETAILS FROM THE OBJECT OF BagDetails
            int[] bagCounts = childBagDetails.getBagCounts();
            double[] bagWeights = childBagDetails.getBagWeights();

            //ADDING BAG DETAILS ON TO RESPECTIVE TEXT VIEWS
            textView1.setText("" + bagCounts[0]);
            textView2.setText("" + bagCounts[1]);
            textView3.setText("" + bagCounts[2]);
            textView4.setText("" + bagCounts[3]);
            textView5.setText("" + bagCounts[4]);
            textView6.setText("" + bagCounts[5]);
            textView7.setText("" + bagCounts[6]);

            textView11.setText("" + bagWeights[0]);
            textView22.setText("" + bagWeights[1]);
            textView33.setText("" + bagWeights[2]);
            textView44.setText("" + bagWeights[3]);
            textView55.setText("" + bagWeights[4]);
            textView66.setText("" + bagWeights[5]);
            textView77.setText("" + bagWeights[6]);

            JSONObject jsonObject = new JSONObject(customer.mData);
            jsonObject.getJSONObject("params").getInt("total_count");
            String t_count = String.valueOf(jsonObject.getJSONObject("params").getInt("total_count") );
            double weight = jsonObject.getJSONObject("params").getDouble("total_weight");
            weight = weight*100;
            weight = Math.round(weight);
            weight = weight /100;
            String t_weight = (String.valueOf(weight ));
            String dateTime[] = customer.mVisitTime.split(" ");
            String manifestNo = dateTime[0]+customer.cID;
            manifestNo = manifestNo.replaceAll("[^a-zA-Z0-9]", "");

            String manifestData = "Manifest#: "+manifestNo+"\n"+
                    "Date&Time: "+ customer.mVisitTimeGMT+"\n"+
                    "Name: "+customer.mName+"\n"+
                    "Bag    "+": "+"Count"+": "+"weight"+"\n";
            if (bagCounts[0] != 0){
                manifestData = manifestData.concat("Blue   "+": "+bagCounts[0]+"  "+bagWeights[0]+"kg\n");
            }
            if (bagCounts[1] != 0){
                manifestData = manifestData.concat("Red    "+": "+bagCounts[1]+"  "+bagWeights[1]+"kg\n");
            }
            if (bagCounts[2] != 0){
                manifestData = manifestData.concat("Yellow "+": "+bagCounts[2]+"  "+bagWeights[2]+"kg\n");
            }
            if (bagCounts[6] != 0){
                manifestData = manifestData.concat("Whites "+": "+bagCounts[6]+"  "+bagWeights[6]+"kg\n");
            }
            if (bagCounts[4] != 0){
                manifestData = manifestData.concat("Sharps "+": "+bagCounts[4]+"  "+bagWeights[4]+"kg\n");
            }
            if (bagCounts[5] != 0){
                manifestData = manifestData.concat("Others "+": "+bagCounts[5]+"  "+bagWeights[5]+"kg\n");
            }
            if (bagCounts[3] != 0){
                manifestData = manifestData.concat("Container "+": "+bagCounts[3]+"  "+bagWeights[3]+"kg\n");
            }
            manifestData = manifestData.concat("Total  "+": "+t_count+"  "+t_weight+"kg\n");

            String customerMobileData = customer.mMobileData;

            final String data = manifestData;
            final String mobileData = customerMobileData;

            manifestButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    generateManifest(data);
                }
            });

            smsButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    generateSmsManifest(data , mobileData);
                }
            });

            boolean isPrintManifestChecked = Boolean.parseBoolean(String.valueOf(pref.getBoolean(constants.print_manifest, false)));
            boolean isSmsManifestChecked = Boolean.parseBoolean(String.valueOf(pref.getBoolean(constants.sms_Manifest, false)));

            if ( !isPrintManifestChecked){
                manifestButton.setVisibility(View.GONE);
                LinearLayout.LayoutParams params = (LinearLayout.LayoutParams) smsButton.getLayoutParams();
                params.setMargins(120, 0, 120, 0) ;//left, top, right, bottom
                smsButton.setLayoutParams(params);
            }

            if ( !isSmsManifestChecked){
                smsButton.setVisibility(View.GONE);
                LinearLayout.LayoutParams params = (LinearLayout.LayoutParams) manifestButton.getLayoutParams();
                params.setMargins(120, 0, 120, 0) ;//left, top, right, bottom
                manifestButton.setLayoutParams(params);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return vi;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return false;
    }

    private void generateManifest(String manifestData){
        activityvisited.sendManifestToPrinter(manifestData+"\n\n");
    }

    private void generateSmsManifest(String manifestData, String mobileData){
        activityvisited.showAlertToSendManifestMsg(manifestData , mobileData);
    }
}
