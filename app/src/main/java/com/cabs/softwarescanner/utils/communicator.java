package com.cabs.softwarescanner.utils;

import android.content.Context;
import android.os.AsyncTask;

import com.cabs.softwarescanner.interfaces.IAsyncResponse;

import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

public class communicator{

    private static final String mTag = "HTTP Communicator";

    public static class PostRequest extends AsyncTask<Context, Void, JSONObject> {

        private String url;
        private JSONObject postBody;
        private JSONObject jsonObject;
        private String session_id = "";

        public IAsyncResponse responseHandler = null;

        public PostRequest(String url, JSONObject jsonObject, String sessionId, IAsyncResponse handler) {
            this.url = url;
            this.postBody = jsonObject;
            this.session_id = sessionId;
            this.responseHandler = handler;
        }

        @Override
        protected JSONObject doInBackground(Context... context) {
            HttpURLConnection connection = null;
            JSONObject responseJsonObject = null;
            try {

                URL posturl = new URL(this.url);
                connection = (HttpURLConnection) posturl.openConnection();
                connection.setRequestMethod("POST");
                connection.setRequestProperty("Content-Type", "application/json");
                connection.setRequestProperty("X-Openerp-Session-Id", session_id);
                connection.setDoInput(true);
                connection.setDoOutput(true);

                //Send request
                DataOutputStream wr = new DataOutputStream(connection.getOutputStream());
                wr.write(postBody.toString().getBytes("UTF-8"));
                wr.flush();
                wr.close();

                InputStream is = connection.getInputStream();
                BufferedReader rd = new BufferedReader(new InputStreamReader(is));
                String line;
                StringBuffer response = new StringBuffer();
                while ((line = rd.readLine()) != null) {
                    response.append(line);
                    response.append('\r');
                }

                rd.close();
                responseJsonObject = new JSONObject(response.toString());
            } catch (Exception e) {
                e.printStackTrace();
            }
            return responseJsonObject;
        }

        @Override
        protected void onPostExecute(JSONObject result) {

            try {
                responseHandler.processFinish(result);
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
    }

    public static class LoginRequest extends AsyncTask<Context, Void, JSONObject> {

        private String url;
        private JSONObject postBody;
        private JSONObject jsonObject;

        IAsyncResponse responseHandler = null;

        public LoginRequest(String url, JSONObject jsonObject, IAsyncResponse handler) {
            this.url = url;
            this.postBody = jsonObject;
            this.responseHandler = handler;
        }

        @Override
        protected JSONObject doInBackground(Context... context) {
            HttpURLConnection connection = null;
            JSONObject responseJsonObject = null;
            try {

                URL posturl = new URL(this.url);
                connection = (HttpURLConnection) posturl.openConnection();
                connection.setRequestMethod("POST");
                connection.setRequestProperty("Content-Type", "application/json");
                connection.setDoInput(true);
                connection.setDoOutput(true);

                //Send request
                DataOutputStream wr = new DataOutputStream(connection.getOutputStream());
                wr.write(postBody.toString().getBytes("UTF-8"));
                wr.flush();
                wr.close();

                InputStream is = connection.getInputStream();
                BufferedReader rd = new BufferedReader(new InputStreamReader(is));
                String line;
                StringBuffer response = new StringBuffer();
                while ((line = rd.readLine()) != null) {
                    response.append(line);
                    response.append('\r');
                }

                rd.close();
                responseJsonObject = new JSONObject(response.toString());

            } catch (Exception e) {
                e.printStackTrace();
            }
            return responseJsonObject;
        }

        @Override
        protected void onPostExecute(JSONObject result) {

            try {
                responseHandler.processFinish(result);
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
    }
}
