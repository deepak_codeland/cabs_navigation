package com.cabs.softwarescanner.utils;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;

import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import com.cabs.softwarescanner.R;
import com.cabs.softwarescanner.customer;

import java.util.ArrayList;

public class adaptercustomer extends ArrayAdapter<customer> implements Filterable{
    private Activity activity;
    private ArrayList<customer> mOriginalValues;
    private ArrayList<customer> mDisplayedValues;

    private static LayoutInflater inflater = null;

    public adaptercustomer(Activity activity, int textViewResourceId, ArrayList<customer> _lCustomer) {
        super(activity, textViewResourceId, _lCustomer);

        try {
            this.activity = activity;
            this.mOriginalValues = _lCustomer;
            this.mDisplayedValues = _lCustomer;
            inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public int getCount() {
        return mDisplayedValues.size();
    }

    public long getItemId(int position) {
        return position;
    }

    public static class ViewHolder {
        public TextView display_name;
    }

    @Override
    public android.widget.Filter getFilter() {
        Filter filter = new Filter() {

            @SuppressWarnings("unchecked")
            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {
                mDisplayedValues = (ArrayList<customer>) results.values; // has the filtered values
                notifyDataSetChanged();  // notifies the data with new filtered values
            }

            @Override
            protected FilterResults performFiltering(CharSequence constraint) {
                FilterResults results = new FilterResults();        // Holds the results of a filtering operation in values
                ArrayList<customer> FilteredArrList = new ArrayList<customer>();

                if (mOriginalValues == null) {
                    mOriginalValues = new ArrayList<customer>(mDisplayedValues); // saves the original data in mOriginalValues
                }

                 // If constraint(CharSequence that is received) is null returns the mOriginalValues(Original) values
                if (constraint == null || constraint.length() == 0) {
                    results.count = mOriginalValues.size();
                    results.values = mOriginalValues;
                } else {
                    constraint = constraint.toString().toLowerCase();
                    for(int i = 0; i < mOriginalValues.size(); i++) {
                        String data = mOriginalValues.get(i).mName;
                        if (data.toLowerCase().startsWith(constraint.toString())) {
                            FilteredArrList.add(new customer( mOriginalValues.get(i).mId, mOriginalValues.get(i).mName));
                        }
                    }
                    // set the Filtered result to return
                    results.count = FilteredArrList.size();
                    results.values = FilteredArrList;
                }
                return results;
            }
        };
        return filter;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        View vi = convertView;
        final ViewHolder holder;
        try {
            if (convertView == null) {
                vi = inflater.inflate(R.layout.list_item, null);
                holder = new ViewHolder();

                holder.display_name = (TextView) vi.findViewById(R.id.customer_name);
                vi.setTag(holder);
            } else {
                holder = (ViewHolder) vi.getTag();
            }

            holder.display_name.setText(mDisplayedValues.get(position).mName);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return vi;
    }
}