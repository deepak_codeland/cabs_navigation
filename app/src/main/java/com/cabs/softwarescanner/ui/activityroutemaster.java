package com.cabs.softwarescanner.ui;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.preference.PreferenceManager;
import android.provider.Settings;
import android.support.v4.app.ActivityCompat;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.Toast;

import com.cabs.softwarescanner.R;
import com.cabs.softwarescanner.constants;
import com.cabs.softwarescanner.customer;
import com.cabs.softwarescanner.database.customerdb;
import com.cabs.softwarescanner.interfaces.IAsyncResponse;
import com.cabs.softwarescanner.service.datasyncerservice;
import com.cabs.softwarescanner.utils.DebugMode;
import com.cabs.softwarescanner.utils.communicator;
import com.cabs.softwarescanner.utils.utility;
import com.google.android.gms.maps.model.LatLng;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.osmdroid.bonuspack.routing.MapQuestRoadManager;
import org.osmdroid.bonuspack.routing.RoadManager;
import org.osmdroid.tileprovider.tilesource.TileSourceFactory;
import org.osmdroid.util.GeoPoint;
import org.osmdroid.views.MapController;
import org.osmdroid.views.MapView;
import org.osmdroid.views.overlay.Polyline;

import java.io.File;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.TreeMap;

import static android.location.LocationProvider.AVAILABLE;
import static android.location.LocationProvider.OUT_OF_SERVICE;
import static android.location.LocationProvider.TEMPORARILY_UNAVAILABLE;


public class activityroutemaster extends Activity implements IAsyncResponse, LocationListener {

    MapView mMap;

    static org.osmdroid.views.overlay.Marker markerForCurrentLocation;
    Button buttonToMoveMapToCurrentLoc, startNavigation;

    private MapController mapController;
    LocationManager locationManager;
    GeoPoint mypoint;
    ArrayList<GeoPoint> arrayListForRoadPoints;
    TreeMap<Integer, GeoPoint> treeMapForDistance_Points;
    ArrayList<Polyline> polylineRoadOverlay;

    ProgressDialog progressDialog;

    private Intent mDataSyncerIntent;
    private LatLng mCurrentLocation;
    customerdb mdb;
    Context context;
    private static final int COARSE_LOCATION_PERMISSION_CODE = 22;
    private static final int LOCATION_PERMISSION_CODE = 23;
    private static final int EXTERNAL_PERMISSION_CODE = 24;
    public static final int SCAN_ACTIVITY = 0;
    public static final int SET_TIME_ACTIVITY = 1;
    public static final int TURN_ON_GPS_ACTIVITY = 2;
    public static final int TURN_OFF_GPS_ACTIVITY = 2;

    CheckBox checkBox_Debug;

    private String mTag = "ActivityRouteMaster";
    ImageButton logoutButton;
    int today_Day;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_routemaster);

        setLogo();

        today_Day = Calendar.getInstance().get(Calendar.DAY_OF_YEAR);
        context = this;
        checkBox_Debug = (CheckBox) findViewById(R.id.checkBox_debug);
        logoutButton = (ImageButton) findViewById(R.id.logout_button);
        logoutButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                forcefulLogout();
            }
        });

        requestWritePermission();
        requestLocationPermission();
        requestCoarseLocationPermission();
        setUpMapIfNeeded();

        arrayListForRoadPoints = new ArrayList<GeoPoint>();
        treeMapForDistance_Points = new TreeMap();
        polylineRoadOverlay = new ArrayList<>();
        buttonToMoveMapToCurrentLoc = (Button) findViewById(R.id.buttonToMoveMapTocurrentLoc);
        startNavigation = (Button) findViewById(R.id.startNavigation);

        startDataSyncer();
        mdb = customerdb.getInstance(this);
        getCustomersOrNot();
        showCustomers();

        checkBox_Debug.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (checkBox_Debug.isChecked()) {
                    DebugMode.setDebugMode(true);
                } else {
                    DebugMode.setDebugMode(false);
                }
            }
        });
    }

    @Override
    protected void onDestroy() {
        if (mdb != null)
            mdb.close();

        if (DebugMode.isDebugMode()) {
            sendLoagcatMail();
        }
        super.onDestroy();

        turnOffGpsUpdates();

    }

    @Override
    protected void onResume() {
        setUpMapIfNeeded();
        arrayListForRoadPoints.clear();

        if (!IsSyncedToday()) {
            today_Day = Calendar.getInstance().get(Calendar.DAY_OF_YEAR);
            mdb.removeOldData(today_Day - constants.DataStorageDuration);
            GetCustomers();
        }
        showCustomers();

        if (arrayListForRoadPoints.size() != 0 /*&& arrayListForRoadPoints.size() != polylineRoadOverlay.size()*/) {
            findNearestCustomer();
        }
        buttonToMoveMapToCurrentLoc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mapController.animateTo(mypoint);
            }
        });

        startNavigation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mMap.getOverlays().clear();
                //mMap.getOverlays().add(markerForCurrentLocation);
                treeMapForDistance_Points = new TreeMap();
                GetCustomers();
                showCustomers();
                findNearestCustomer();
            }
        });

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        //locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 1, this);

        super.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    private void requestLocationPermission() {

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, LOCATION_PERMISSION_CODE);
        }
    }

    private void requestCoarseLocationPermission() {

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_COARSE_LOCATION}, COARSE_LOCATION_PERMISSION_CODE);
        }
    }

    private void requestWritePermission() {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, EXTERNAL_PERMISSION_CODE);
        }

    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    private void setUpMapIfNeeded() {
        if (mMap == null) {

            mMap = (MapView) findViewById(R.id.mapview);
            mMap.setBuiltInZoomControls(true);
            mMap.setMultiTouchControls(true);

            mMap.setClickable(true);
            mMap.setUseDataConnection(true);

            mypoint = new GeoPoint(12.9532, 77.5835);
            mapController = (MapController) mMap.getController();
            mMap.setTileSource(TileSourceFactory.MAPNIK);
            mapController.setZoom(18);
            mapController.setCenter(mypoint);

            markerForCurrentLocation = new org.osmdroid.views.overlay.Marker(mMap);
            markerForCurrentLocation.setAnchor(org.osmdroid.views.overlay.Marker.ANCHOR_CENTER, org.osmdroid.views.overlay.Marker.ANCHOR_BOTTOM);
            markerForCurrentLocation.setIcon(getResources().getDrawable(R.drawable.blue));
            markerForCurrentLocation.setSnippet(getString(R.string.current_location));

            locationManager = (LocationManager) getSystemService(context.LOCATION_SERVICE);

            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                return;
            }
            Location currentLocation = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
            if (currentLocation != null) {
                mypoint = new GeoPoint(currentLocation.getLatitude(), currentLocation.getLongitude());
            }
            markerForCurrentLocation.setPosition(mypoint);
            mMap.getOverlays().add(markerForCurrentLocation);
            locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 1, 0, this);
            locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0, 0, this);
        }

    }

    public void showVisited(View view) {

        Intent deliverIntent = new Intent(context, activityvisited.class);
        deliverIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(deliverIntent);
    }

    public void getCustomersOrNot() {

        if (!IsSyncedToday()) {
            today_Day = Calendar.getInstance().get(Calendar.DAY_OF_YEAR);
            mdb.removeOldData(today_Day - constants.DataStorageDuration);
            GetCustomers();
        }
    }

    public void loadCustomers(View view) {
        mdb.removeNonVisitedCustomer();
        GetCustomers();
    }

    public void showsAllCustomertoVisit(View view) {

        Intent deliverIntent = new Intent(context, activitycustomers.class);
        deliverIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(deliverIntent);
    }

    public void forcollection(View view) {

        if (!utility.isTimeAutomatic(this)) {
            askUserToSetTime();

        } else {
            try {
                Intent intent = new Intent(this, activityscanQR.class);
                startActivityForResult(intent, SCAN_ACTIVITY);

            } catch (ActivityNotFoundException anfe) {
                anfe.printStackTrace();

                Intent newintent = new Intent(this, activitycollector.class);
                startActivity(newintent);
            }
        }

    }

    public void forcefulLogout() {
        SharedPreferences pref = getSharedPreferences(constants.settings_filename, MODE_PRIVATE);
        utility.resetLastLoginDay(pref);
        finish();
        moveTaskToBack(true);
        System.exit(0);
    }

    public void onActivityResult(int requestCode, int resultCode, Intent intent) {

        switch (requestCode) {
            case SCAN_ACTIVITY:
                try {
                    String contents = intent.getStringExtra("RESULT_STRING");
                    if (contents == null) {
                        // Handle cancel
                        Toast toast = Toast.makeText(this, "Failed to read the QR Code", Toast.LENGTH_LONG);
                        toast.setGravity(Gravity.TOP, 25, 400);
                        toast.show();
                        return;
                    } else if (resultCode != RESULT_OK) {
                        // Handle cancel
                        Toast toast = Toast.makeText(this, "Scan was Cancelled!", Toast.LENGTH_LONG);
                        toast.setGravity(Gravity.TOP, 25, 400);
                        toast.show();
                        return;
                    }

                    // Handle successful scan default initiation to the collector class..
                    Intent newIntent = new Intent(this, activitycollector.class);

                    String tokens[] = contents.split("@");
                    customer custObj = new customer(mdb);
                    if (mCurrentLocation == null) {
                        mCurrentLocation = new LatLng(0.0, 0.0);
                    }
                    custObj.mLatitude = mypoint.getLatitude();
                    custObj.mLongitude = mypoint.getLongitude();
                    custObj.mId = Integer.parseInt(tokens[0]);
                    custObj.mName = tokens[1];
                    newIntent.putExtra("customer", custObj);
                    startActivity(newIntent);

                } catch (Exception e) {
                    e.printStackTrace();

                    Toast toast = Toast.makeText(context, "Invalid QR Code", Toast.LENGTH_LONG);
                    toast.setGravity(Gravity.TOP, 25, 400);
                    toast.show();
                }
                break;
        }
    }

    private void startDataSyncer() {

        if (mDataSyncerIntent == null) {
            mDataSyncerIntent = new Intent(this, datasyncerservice.class);
            startService(mDataSyncerIntent);
        }
    }

    public boolean IsSyncedToday() {
        SharedPreferences pref = getSharedPreferences(constants.settings_filename, MODE_PRIVATE);
        SharedPreferences.Editor editor = pref.edit();
        int day = pref.getInt(constants.lastsyncday, 0);
        int today = Calendar.getInstance().get(Calendar.DAY_OF_YEAR);

        if (today != day) {
            editor.putInt(constants.lastsyncday, today);
            editor.apply();
            return false;
        }
        return true;
    }


    public void GetCustomers() {

        try {
            SharedPreferences sharedPrefs = PreferenceManager.getDefaultSharedPreferences(this);
            String vehicle_id = sharedPrefs.getString(constants.vehicle_id, "0");
            SharedPreferences pref = getSharedPreferences(constants.settings_filename, MODE_PRIVATE);
            String session_id = pref.getString(constants.session_id, "");

            JSONObject jsonObject = new JSONObject();
            JSONObject json = new JSONObject();
            jsonObject.put("vehicle_id", vehicle_id);
            json.put("params", jsonObject);

            communicator.PostRequest postRequest = new communicator.PostRequest(constants.CUSTOMER_URL, json, session_id, this);
            postRequest.execute();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    @Override
    public void processFinish(JSONObject jsonObject) {

        try {
            if (!IsSyncedToday()) {
                today_Day = Calendar.getInstance().get(Calendar.DAY_OF_YEAR);
                mdb.removeOldData(today_Day - constants.DataStorageDuration);
            }

            JSONObject json;
            if (jsonObject != null) {
                json = jsonObject.optJSONObject("result");

                saveRouteId(json.getInt("route"));
                JSONArray jsonArray = json.getJSONArray("customers");

                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject jsonCustomer = jsonArray.getJSONObject(i);
                    int id = jsonCustomer.optInt("id");
                    String name = jsonCustomer.optString("name");
                    String str = mdb.getCustomer(id);
                    if (str.isEmpty()) {
                        mdb.putCustomer(id, name, jsonCustomer.toString(), false, false, null, -1, jsonCustomer.toString());
                    }
                }

            } else {
                Toast.makeText(this, "Failed to Get List Customer.", Toast.LENGTH_LONG).show();
            }

        } catch (JSONException e) {
            e.printStackTrace();
            try {
                SharedPreferences pref = getSharedPreferences(constants.settings_filename, MODE_PRIVATE);
                JSONObject json = jsonObject.optJSONObject("result");
                utility.saveSession(pref, json.getString(constants.session_id), json.optLong(constants.session_expiry));

            } catch (JSONException ex) {
                ex.printStackTrace();
                Log.v(mTag, "Bad json response");
            } catch (NullPointerException exc) {
                exc.printStackTrace();
                Log.v(mTag, "Failed to login");
            }
            Toast.makeText(this, "Go to Settings and check Route ID", Toast.LENGTH_SHORT).show();
        } catch (NullPointerException e) {
            e.printStackTrace();
            SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
            utility.Login(getApplicationContext(), pref, this);
        }
    }

    private boolean saveRouteId(int route_id) {

        SharedPreferences settings = getSharedPreferences(constants.settings_filename, MODE_PRIVATE);
        SharedPreferences.Editor editor = settings.edit();
        editor.putInt(constants.route_id, route_id);

        editor.apply();
        return true;
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public void showMarker(double Latitude, double Longitude, String name, int resourceId) {

        GeoPoint customerPoint = new GeoPoint(Latitude, Longitude);

        org.osmdroid.views.overlay.Marker marker = new org.osmdroid.views.overlay.Marker(mMap);
        marker.setPosition(customerPoint);
        marker.setAnchor(org.osmdroid.views.overlay.Marker.ANCHOR_CENTER, org.osmdroid.views.overlay.Marker.ANCHOR_BOTTOM);

        marker.setIcon(getResources().getDrawable(resourceId));
        marker.setSnippet(name);

        mMap.getOverlays().add(marker);
        if (!(Latitude == 0.0 && Longitude == 0.0)){
            arrayListForRoadPoints.add(customerPoint);
        }
    }

    public void showCustomers() {
        Cursor c = null;

        try {
            mdb = customerdb.getInstance(this);
            c = mdb.getCustomers(false);
            while (c.moveToNext()) {
                try {
                    JSONObject jsonCustomer = new JSONObject(c.getString(2));
                    showMarker(jsonCustomer.getDouble("lat"), jsonCustomer.getDouble("lon"),
                            jsonCustomer.getString("name"), R.drawable.green);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (c != null && !c.isClosed())
                c.close();
        }
    }

    public void findNearestCustomer(){
        if (mMap != null){
            new ShowNavigationAsyncTask().execute();
        }
    }


    public class ShowNavigationAsyncTask extends AsyncTask<Void, Void, Void> {

        @Override
        protected void onPreExecute() {
            //super.onPreExecute();
            progressDialog = new ProgressDialog(context);
            progressDialog.setTitle("YOUR NAVIGATION");
            progressDialog.setMessage("is downloading");
            progressDialog.show();
        }

        @Override
        protected Void doInBackground(Void... params) {

            int nearestPoint = 0; //distance between two points
            GeoPoint startPoint = mypoint;
            GeoPoint endPoint = null;
            ArrayList<GeoPoint> roadPoints = new ArrayList<>();
            RoadManager roadManager = new MapQuestRoadManager("cuAt9LzDGexGlRhkOA7fbozqQGDcvncl");
            if (arrayListForRoadPoints.size() > 0){
                for (int i = 0; i < (arrayListForRoadPoints.size()); i++){
                    int distance = startPoint.distanceTo(arrayListForRoadPoints.get(i));
                    treeMapForDistance_Points.put(distance , arrayListForRoadPoints.get(i));
                }
                polylineRoadOverlay.clear();
                ArrayList<GeoPoint> waypoints = new ArrayList<>(treeMapForDistance_Points.values());
                startPoint = mypoint;
                endPoint = waypoints.get(0);
                roadPoints.clear();
                roadPoints.add(startPoint);
                roadPoints.add(endPoint);
                Polyline polyline = RoadManager.buildRoadOverlay(roadManager.getRoad(roadPoints));
                polylineRoadOverlay.add(polyline);
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            //super.onPostExecute(aVoid);
            if(progressDialog.isShowing()) {
                progressDialog.dismiss();
            }
            mMap.getOverlays().add(markerForCurrentLocation);
            if (arrayListForRoadPoints.size() > 0){
                for (int i = 0; i < polylineRoadOverlay.size(); i++){
                    mMap.getOverlays().add(polylineRoadOverlay.get(i));
                }
            }
            mapController.animateTo(mypoint);
        }
    }

    //This method will be called when the user will tap on allow or deny
    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case EXTERNAL_PERMISSION_CODE: {
                //If permission is granted
                if (grantResults.length > 0 && grantResults[0] != PackageManager.PERMISSION_GRANTED) {
                    Toast.makeText(this, "Oops you just denied the permission", Toast.LENGTH_LONG).show();
                }
                return;
            }
            case LOCATION_PERMISSION_CODE: {
                //If permission is granted
                if (grantResults.length > 0 && grantResults[0] != PackageManager.PERMISSION_GRANTED) {
                    Toast.makeText(this, "Oops you just denied the permission", Toast.LENGTH_LONG).show();
                }
                return;
            }
            case COARSE_LOCATION_PERMISSION_CODE: {
                //If permission is granted
                if (grantResults.length > 0 && grantResults[0] != PackageManager.PERMISSION_GRANTED) {
                    Toast.makeText(this, "Oops you just denied the permission", Toast.LENGTH_LONG).show();
                }
            }

        }
    }

    @Override
    public void onLocationChanged(Location location) {
        mypoint = null;
        mypoint = new GeoPoint(location.getLatitude(), location.getLongitude());
        markerForCurrentLocation.setPosition(mypoint);
        mMap.getOverlays().add(markerForCurrentLocation);
        mMap.invalidate();
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {
        switch (status) {
            case OUT_OF_SERVICE:
                Toast.makeText(this, "GPS is out of service", Toast.LENGTH_SHORT).show();
                break;
            case TEMPORARILY_UNAVAILABLE:
                Toast.makeText(this, "GPS is temporarily unavailable", Toast.LENGTH_SHORT).show();
                break;
            case AVAILABLE:
                //Toast.makeText(this, "GPS is turned on", Toast.LENGTH_SHORT).show();
                break;
        }
    }

    @Override
    public void onProviderEnabled(String provider) {
        Toast.makeText(this, "You turned on the GPS", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onProviderDisabled(String provider) {
        askUserToTurnOnGPS();
    }

    public void sendLoagcatMail() {

        if (activitysplash.isExternalStorageWritable()) {

            File appDirectory = new File(Environment.getExternalStorageDirectory() + "/CollectionAutomationLog");
            File logDirectory = new File(appDirectory + "/log");
            File logFile = new File(logDirectory, "logcat" + ".txt");

            if (logFile.exists()) {
                Intent emailIntent = new Intent(Intent.ACTION_SEND);
                emailIntent.setType("vnd.android.cursor.dir/email");
                String to[] = {"sharath.h@codeland.in"};
                emailIntent.putExtra(Intent.EXTRA_EMAIL, to);
                emailIntent.putExtra(Intent.EXTRA_STREAM, Uri.parse(String.valueOf(logFile.toURI())));
                emailIntent.putExtra(Intent.EXTRA_SUBJECT, "Log files");
                emailIntent.putExtra(Intent.EXTRA_TEXT, "App is not working properly,please find the attached log file.");
                startActivity(Intent.createChooser(emailIntent, "Send email..."));
            }
        }
    }

    public void askUserToSetTime() {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(context);
        dialogBuilder.setTitle(R.string.set_time_zone);
        dialogBuilder.setPositiveButton(R.string.go_to_settings, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                startActivityForResult(new Intent(android.provider.Settings.ACTION_DATE_SETTINGS), SET_TIME_ACTIVITY);
            }
        });

        AlertDialog alertDialog = dialogBuilder.create();
        alertDialog.setCanceledOnTouchOutside(false);
        alertDialog.setCancelable(false);
        if (!activityroutemaster.this.isFinishing()) {
            alertDialog.show();
        }
    }

    public void askUserToTurnOnGPS() {

        Toast.makeText(context, "Turn On GPS!", Toast.LENGTH_SHORT).show();
        Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
        startActivityForResult(intent, TURN_ON_GPS_ACTIVITY);
    }

    public void turnOffGpsUpdates() {

        if (locationManager != null && locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                return;
            }
            try{
                locationManager.removeUpdates(this);
            }catch (Exception e){
                e.printStackTrace();
            }

        }

    }

    public void setLogo(){

        SharedPreferences sharedPrefs = PreferenceManager.getDefaultSharedPreferences(this);
        String logo_selected = sharedPrefs.getString(constants.logo_check, "CodeLand");
        ImageView imageView = (ImageView)findViewById(R.id.imageViewBack);

        switch (logo_selected){
            case constants.CodeLand:
                imageView.setImageResource(R.drawable.back_codeland);
                break;
            case constants.Medicare:
                imageView.setImageResource(R.drawable.back_medicare);
                break;
            case constants.Maridi:
                imageView.setImageResource(R.drawable.back_maridi);
                break;
            case constants.AECS:
                imageView.setImageResource(R.drawable.back_codeland);
                break;
            default:
                imageView.setImageResource(R.drawable.back_codeland);
                break;

        }
    }

}
