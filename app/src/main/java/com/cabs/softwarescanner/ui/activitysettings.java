package com.cabs.softwarescanner.ui;

import android.os.Bundle;
import android.preference.PreferenceActivity;
import com.cabs.softwarescanner.R;

public class activitysettings extends PreferenceActivity {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        addPreferencesFromResource(R.xml.settings);
    }
}
