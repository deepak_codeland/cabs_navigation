package com.cabs.softwarescanner.ui;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.preference.PreferenceManager;
import android.support.v4.app.ActivityCompat;
import android.telephony.SmsManager;
import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.EditText;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.cabs.softwarescanner.BagDetails;
import com.cabs.softwarescanner.R;
import com.cabs.softwarescanner.bluetooth.activitydevicelist;
import com.cabs.softwarescanner.bluetooth.baseactivity;
import com.cabs.softwarescanner.constants;
import com.cabs.softwarescanner.customer;
import com.cabs.softwarescanner.database.customerdb;
import com.cabs.softwarescanner.utils.adaptervisitedcustomer;
import com.cabs.softwarescanner.weighingscale;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Set;
import java.util.UUID;

import static com.cabs.softwarescanner.ui.activitysplash.isExternalStorageWritable;


public class activityvisited extends baseactivity {

    static final int REQUEST_CONNECT_DEVICE = 3;
    static final int REQUEST_ENABLE_BT = 4;
    private String deviceName;
    private static TextView bluetoothstatus;
    private static TextView autoresponsestatus;
    private static devicebluetoothconnector connector;
    private static devicebluetoothconnector backUpConnector;  //Backup connector for resuming bluetooth-connection
    private String BackUpdeviceName;
    public static BluetoothResponseHandler mHandler;
    private static final int NO_OF_BAGROWS = 7;

    static BluetoothAdapter mBluetoothAdapter;
    static BluetoothSocket mmSocket;
    static BluetoothDevice mmDevice;
    static boolean connected = false;

    // android built in classes for bluetooth operations
    static OutputStream mmOutputStream;
    static InputStream mmInputStream;
    static Thread workerThread;

    static byte[] readBuffer;
    static int readBufferPosition;
    int counter;
    static volatile boolean stopWorker;

    adaptervisitedcustomer adbCustomer;
    customerdb mdb;
    Context context;
    static Context sContext;
    static Activity sActivity;
    ExpandableListView expandableListView;
    ArrayList<customer> listDataHeaderVisitedCustomer;
    HashMap<String, ArrayList<BagDetails>> listDataChildBagDetails;
    TextView textViewToShowVisitedCount;

    private static String m_Text = "";
    static String msg = "";

    private static final int SMS_PERMISSION_CODE = 30;
    private static final int FOR_SMS = 1;
    private String mTag = "ActivityVisited";
    private static String manifestString ;
    private static String messageToDisplay = "";
    private static AlertDialog alertDialogConnectPrinter;
    private static AlertDialog.Builder builderConnectPrinter;
    private static BluetoothDevice bluetoothDevicePrinter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_visited);

        setLogo();

        context = this;
        sContext = this;
        sActivity = this;

        EditText inputSearch = (EditText)findViewById(R.id.visitedSearch);
        expandableListView = (ExpandableListView) findViewById(R.id.visited_list_view);

        if (mHandler == null) mHandler = new activityvisited.BluetoothResponseHandler(this);
        else mHandler.setTarget(this);
        bluetoothstatus = (TextView) findViewById(R.id.bluetoothstatus);
        autoresponsestatus = (TextView) findViewById(R.id.autoresponsestatus);
        autoresponsestatus.setText("");

        prepareListData();

        adbCustomer = new adaptervisitedcustomer(this, R.id.visited_customer_name, getCustomers(), listDataChildBagDetails);
        expandableListView.setAdapter(adbCustomer);
        textViewToShowVisitedCount = (TextView) findViewById(R.id.textViewToShowVisitedCount);
        textViewToShowVisitedCount.setText(getCustomers().size()+"");
        textViewToShowVisitedCount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendDataToMail();
            }
        });

        inputSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
                activityvisited.this.adbCustomer.getFilter().filter(charSequence);
            }

            @Override
            public void afterTextChanged(Editable editable) {
            }
        });

        requestSmsPermission();
    }

    /*
     * Preparing the list data for visited customer's bag details
     */

    public int prepareListData() {
        listDataHeaderVisitedCustomer = new ArrayList<>();
        listDataChildBagDetails = new HashMap<>();

        listDataHeaderVisitedCustomer = getCustomers();
        int cnt = 0;
        // Adding child data that is bag details
        for (int i = 0; i < listDataHeaderVisitedCustomer.size(); i++){
            ArrayList<BagDetails> arrayListForBagdetails = new ArrayList<>();
            int[] bagCount = new int[NO_OF_BAGROWS];
            double[] bagWeight = new double[NO_OF_BAGROWS];
            customer customer = listDataHeaderVisitedCustomer.get(i);
            String name = customer.mName;
            try {
                JSONObject jsonObject = new JSONObject(customer.mData);
                JSONArray jsonArray = jsonObject.getJSONObject("params").getJSONArray("categories");

                for(int n = 0; n < NO_OF_BAGROWS; n++)
                {
                    JSONObject object = jsonArray.getJSONObject(n);
                    int category = object.getInt("category_id");
                    bagCount[category-1] = object.getInt("category_count");
                    bagWeight[category-1] = object.getDouble("category_weight");
                }

                BagDetails bagDetails = new BagDetails(name, bagCount, bagWeight);
                arrayListForBagdetails.add(bagDetails);
                listDataChildBagDetails.put(name+ ":\n"+ customer.mVisitTime, arrayListForBagdetails);
                cnt++;
            } catch (JSONException e) {
                e.printStackTrace();
            }  catch (Exception e){
                e.printStackTrace();
            }
        }
        return cnt;
    }


    public ArrayList<customer> getCustomers(){

        Cursor c = null;
        ArrayList<customer> retVal = null;

        try {
            mdb = customerdb.getInstance(this);
            c = mdb.getCustomers(true);

            retVal =new ArrayList<>();

            while (c.moveToNext()) {
                customer custobj = new customer();
                custobj.mId = c.getInt(0);
                custobj.mName = c.getString(1);
                custobj.mData = c.getString(2);
                custobj.mVisitTime = c.getString(3);
                custobj.cID = c.getInt(4);
                custobj.mMobileData = c.getString(5);
                if (custobj.mData.contains("params")){
                    retVal.add(custobj);
                }
            }

        }catch(Exception e){
            e.printStackTrace();
        }finally {
            if(c!=null && !c.isClosed())
                c.close();
        }

        return retVal;
    }

    @Override
    protected void onDestroy(){
        if( mdb != null)
            mdb.close();

        super.onDestroy();
    }

    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);
        if(Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN_MR2) {
            expandableListView.setIndicatorBounds(expandableListView.getRight()- 80, expandableListView.getWidth());
        } else {
            expandableListView.setIndicatorBoundsRelative(expandableListView.getRight()- 80, expandableListView.getWidth());

        }
    }


    public void sendDataToMail() {

        customerdb db = customerdb.getInstance(context);
        String unSyncCollectionBody = "";
        ArrayList<Integer> customerListUnSynced = new ArrayList<>();
        Cursor unSync_c = db.getCustomerstoSync();

        String syncCollectionBody = "";
        ArrayList<Integer> customerListAlreadySynced = new ArrayList<>();
        Cursor sync_c = db.getCustomersAlreadySynced();

        if (unSync_c != null) {
            if (unSync_c.getCount() == 0) {
                Toast.makeText(context, "All data is already synced !", Toast.LENGTH_LONG).show();
            } else {
                unSyncCollectionBody = unSyncCollectionBody.concat("Unsynced data count : "+unSync_c.getCount()+"\n\n");
                while (unSync_c.moveToNext()) {
                    unSyncCollectionBody = unSyncCollectionBody.concat(unSync_c.getString(3) + "\n" + unSync_c.getString(2) + "\n\n");
                    customerListUnSynced.add(unSync_c.getInt(0));
                }
            }
        }
        if (sync_c != null) {
            if (sync_c.getCount() != 0) {
                syncCollectionBody = syncCollectionBody.concat("Synced data count : "+sync_c.getCount()+"\n\n");
                while (sync_c.moveToNext()) {
                    syncCollectionBody = syncCollectionBody.concat(sync_c.getString(3) + "\n" + sync_c.getString(2) + "\n\n");
                    customerListAlreadySynced.add(sync_c.getInt(0));
                }
            }
        }
        if (isExternalStorageWritable()) {
            try {
                File appDirectory = new File(Environment.getExternalStorageDirectory() + "/DataCollection");
                File dataDirectory = new File(appDirectory + "/Data");
                File unSyncedDataFile = new File(dataDirectory, "unSyncedData" + ".txt");
                File syncedDataFile = new File(dataDirectory, "syncedData" + ".txt");

                if (!appDirectory.exists()) {
                    appDirectory.mkdir();
                }

                if (!dataDirectory.exists()) {
                    dataDirectory.mkdir();
                }

                if (unSyncedDataFile.exists()) {
                    unSyncedDataFile.delete();
                }

                if (syncedDataFile.exists()) {
                    syncedDataFile.delete();
                }

                FileWriter writer_unSynced = new FileWriter(unSyncedDataFile);
                writer_unSynced.write(unSyncCollectionBody);
                writer_unSynced.flush();
                writer_unSynced.close();

                FileWriter writer_synced = new FileWriter(syncedDataFile);
                writer_synced.write(syncCollectionBody);
                writer_synced.flush();
                writer_synced.close();

                ArrayList<Uri> uriArrayList = new ArrayList<>();
                uriArrayList.add(Uri.parse(String.valueOf(syncedDataFile.toURI())));
                uriArrayList.add(Uri.parse(String.valueOf(unSyncedDataFile.toURI())));


                String vehicle_id = "";
                try {
                    SharedPreferences sharedPrefs = PreferenceManager.getDefaultSharedPreferences(this);
                    vehicle_id = sharedPrefs.getString(constants.vehicle_id, "0");

                } catch (Exception e) {
                    e.printStackTrace();
                }

                Intent emailIntent = new Intent(Intent.ACTION_SEND_MULTIPLE);
                emailIntent.setType("vnd.android.cursor.dir/email");
                String to[] = {"alert@codeland.in"};
                emailIntent.putExtra(Intent.EXTRA_EMAIL, to);
                emailIntent.putParcelableArrayListExtra(Intent.EXTRA_STREAM, uriArrayList);
                emailIntent.putExtra(Intent.EXTRA_SUBJECT, "Collection to sync for Route ID : " + vehicle_id);
                emailIntent.putExtra(Intent.EXTRA_TEXT, "All data was not synced,\nPlease find the attached data files.");
                startActivity(Intent.createChooser(emailIntent, "Send data via email..."));

            } catch (IOException e) {
                e.printStackTrace();
            }

        }
    }

    private static class BluetoothResponseHandler extends Handler {
        private WeakReference<activityvisited> mActivity;
        static String data = "";

        BluetoothResponseHandler(activityvisited activity) {
            mActivity = new WeakReference<>(activity);
        }

        void setTarget(activityvisited target) {
            mActivity.clear();
            mActivity = new WeakReference<>(target);
        }

        @Override
        public void handleMessage(Message msg) {
            activityvisited activity = mActivity.get();

            if (activity != null) {
                switch (msg.what) {
                    case MESSAGE_STATE_CHANGE:

                        Log.i("devicecontrol", "MESSAGE_STATE_CHANGE: " + msg.arg1);
                        switch (msg.arg1) {
                            case devicebluetoothconnector.STATE_CONNECTED:
                                //Add data to the status bar
                                break;
                            case devicebluetoothconnector.STATE_CONNECTING:
                                break;
                            case devicebluetoothconnector.STATE_NONE:
                                //Add data to the status bar
                                break;
                        }
                        break;

                    case MESSAGE_READ:
                        data = data.concat((String) msg.obj);
                        //activity.setResponse(data);
//                        messageToDisplay = "Connected";
//                        activity.setResponse(messageToDisplay);
                        break;
                    case MESSAGE_DEVICE_NAME:
                        //activity.setDeviceName((String) msg.obj);
                        break;

                    case MESSAGE_WRITE:
                        // stub
                        break;

                    case MESSAGE_TOAST:
                        data = data.concat(msg.toString());
                        messageToDisplay = "Paired";
                        activity.setResponse(messageToDisplay);
                        stopConnection();
                        break;
                }
            }
        }
    }

    void setResponse(String response) {
        this.autoresponsestatus.setText(""+response);
    }

    private static void stopConnection() {
        if (connector != null) {
            connector.stop();
            connector = null;
//            deviceName = null;
            try {
                sActivity.runOnUiThread(new Runnable() {
                    public void run() {
                        messageToDisplay = "" ;
                        autoresponsestatus.setText(messageToDisplay);
                    }
                });
            }catch (Exception e){
                e.printStackTrace();
            }

        }
    }

    public static void sendManifestToPrinter(String manifest){

        messageToDisplay = "Connecting... Please wait !" ;
        autoresponsestatus.setText(messageToDisplay);
        manifestString = manifest;
        new printAsyncTask().execute();
    }

    // Tries to open a connection to the bluetooth printer device
    static boolean openBT() throws IOException {

        //closeBT();
        try {

            // Standard SerialPortService ID
            if (mmSocket == null) {
                UUID uuid = UUID.fromString("00001101-0000-1000-8000-00805f9b34fb");
                mmSocket = mmDevice.createRfcommSocketToServiceRecord(uuid);
            }
            if (!mmSocket.isConnected()) {
                mmSocket.connect();
            }
            mmOutputStream = mmSocket.getOutputStream();
            mmInputStream = mmSocket.getInputStream();

            beginListenForData();

            messageToDisplay = "Bluetooth Opened" ;
            sendData(manifestString);
            return true;

        } catch (Exception e) {
            e.printStackTrace();
            messageToDisplay = "Failed to connect";
            closeBT();
            return false;
        }
    }

    // After opening a connection to bluetooth printer device,
// we have to listen and check if a data were sent to be printed.
    static void beginListenForData() {
        try {
            final Handler handler = new Handler();

            // This is the ASCII code for a newline character
            final byte delimiter = 10;

            stopWorker = false;
            readBufferPosition = 0;
            readBuffer = new byte[1024];

            workerThread = new Thread(new Runnable() {
                public void run() {
                    while (!Thread.currentThread().isInterrupted()
                            && !stopWorker) {

                        try {

                            int bytesAvailable = mmInputStream.available();
                            if (bytesAvailable > 0) {
                                byte[] packetBytes = new byte[bytesAvailable];
                                mmInputStream.read(packetBytes);
                                for (int i = 0; i < bytesAvailable; i++) {
                                    final byte b = packetBytes[i];
                                    if (b == delimiter) {
                                        byte[] encodedBytes = new byte[readBufferPosition];
                                        System.arraycopy(readBuffer, 0,
                                                encodedBytes, 0,
                                                encodedBytes.length);
                                        final String data = new String(
                                                encodedBytes, "US-ASCII");
                                        readBufferPosition = 0;

                                        handler.post(new Runnable() {
                                            public void run() {
                                                autoresponsestatus.setText(data);
                                            }
                                        });
                                    }
                                    else {
                                        readBuffer[readBufferPosition++] = b;
                                    }
                                }
                            }

                        } catch (IOException ex) {
                            stopWorker = true;
                        }

                    }
                }
            });

            workerThread.start();
        } catch (NullPointerException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    static void sendData(String msg) throws IOException {
        try {

            mmOutputStream.write(msg.getBytes());
            //mmOutputStream.flush();
            messageToDisplay = "Manifest is generated!" ;

//            new Handler().postDelayed(new Runnable() {
//                @Override
//                public void run() {
//                    try {
//                        closeBT();
//                    } catch (IOException e) {
//                        e.printStackTrace();
//                    }
//                }
//            }, 10000);
            return;
        } catch (NullPointerException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    // Close the connection to bluetooth printer.
    static void closeBT() throws IOException {
        try {
            stopWorker = true;
            if (mmOutputStream != null){
                try{
                    mmOutputStream.close();
                }catch (IOException e){
                    e.printStackTrace();
                }
            }
            if (mmInputStream != null){
                try{
                    mmInputStream.close();
                }catch (IOException e){
                    e.printStackTrace();
                }
            }
            if (mmSocket != null){
                try{
                    mmSocket.close();
                    mmSocket = null;
                }catch (IOException e){
                    e.printStackTrace();
                }
            }
            messageToDisplay = "";
            stopConnection();

        } catch (NullPointerException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    // This will find a bluetooth printer device
    static boolean findBT() {

        boolean found = false;
        try {
            mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();

            if (mBluetoothAdapter == null) {
                messageToDisplay = "Printer not available";
            }

            if (!mBluetoothAdapter.isEnabled()) {

                try {
                    sActivity.runOnUiThread(new Runnable() {
                        public void run() {
                            messageToDisplay = "Bluetooth is OFF!";
                            autoresponsestatus.setText(messageToDisplay);
                            bluetoothstatus.setText("");
                            Intent enableBluetooth = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
                            sContext.startActivity(enableBluetooth);
                        }
                    });
                }catch (Exception e){
                    e.printStackTrace();
                }
            }

            Set<BluetoothDevice> pairedDevices = mBluetoothAdapter
                    .getBondedDevices();
//            if (pairedDevices.size() > 0) {
//                for (BluetoothDevice device : pairedDevices) {

                    if (bluetoothDevicePrinter != null){
                        mmDevice = bluetoothDevicePrinter;
                        found = true;
                        openBT();
                    }else {
                        messageToDisplay = "Connect to Printer";
                        sActivity.runOnUiThread(new Runnable() {
                            public void run() {
                                askUserToPair();
                                autoresponsestatus.setText(messageToDisplay);
                                bluetoothstatus.setText("");
                            }
                        });
                    }

//            }
        } catch (NullPointerException e) {
            e.printStackTrace();
            autoresponsestatus.setText(messageToDisplay);
        } catch (Exception e) {
            e.printStackTrace();
            autoresponsestatus.setText(messageToDisplay);
        }
        return found;
    }


    public static void showAlertToSendManifestMsg(final String msgManifest, final String mobileData){

        try {
            JSONObject jsonObject = new JSONObject(mobileData);
            if (jsonObject != null){
                String mobileNumbers = jsonObject.getString("mobile");
                String[] mobileNumber = mobileNumbers.split(",");
                String mobile = mobileNumber[0].replaceAll("[^a-zA-Z0-9]", "");
                if (mobile.length() != 10){
                    sendSmsByEditingNumber(msgManifest);
                }else {
                    try {
                        long mobileNo = Long.parseLong(mobile);
                        sendSmsUsingGivenNumber(msgManifest, mobile);
                    }catch (Exception e){
                        sendSmsByEditingNumber(msgManifest);
                    }
                }
            }
        }

        catch (Exception e){
            e.printStackTrace();
            sendSmsByEditingNumber(msgManifest);
        }
    }

    public static void sendSmsUsingGivenNumber (final String msgManifest, final String mobileNo){

        android.support.v7.app.AlertDialog.Builder builder = new android.support.v7.app.AlertDialog.Builder(sContext);
        builder.setTitle("Send Manifest to");

        final TextView textViewShowNumber = new TextView(sContext);
        textViewShowNumber.setText(mobileNo);
        textViewShowNumber.setGravity(Gravity.CENTER);
        textViewShowNumber.setTextSize(30.0f);
        builder.setView(textViewShowNumber);
        builder.setCancelable(false);
        msg = msgManifest;
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                m_Text = mobileNo;
                sendManifestMessage(msg);

            }
        });
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
                dialog.dismiss();
            }
        });
        android.support.v7.app.AlertDialog alertDialog = builder.create();
        alertDialog.show();
    }

    public static void sendSmsByEditingNumber(final String msgManifest){

        android.support.v7.app.AlertDialog.Builder builder = new android.support.v7.app.AlertDialog.Builder(sContext);
        builder.setTitle("Send Manifest");

        final EditText input = new EditText(sContext);
        input.setHint("Enter mobile no...");
        input.setInputType(InputType.TYPE_CLASS_PHONE);
        builder.setView(input);
        builder.setCancelable(false);
        msg = msgManifest;
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                try {
                    m_Text = input.getText().toString();
                    long m_number = Long.parseLong(m_Text);
                    if (m_Text.length() == 10) {

                        sendManifestMessage(msg);
                    } else {
                        Toast.makeText(sContext, "Please enter correct 10-digit number",
                                Toast.LENGTH_LONG).show();
                    }

                } catch (Exception e) {
                    Toast.makeText(sContext, "Please enter correct 10-digit number",
                            Toast.LENGTH_LONG).show();
                }
            }
        });
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
                dialog.dismiss();
            }
        });
        android.support.v7.app.AlertDialog alertDialog = builder.create();
        alertDialog.show();
    }

    private static void sendManifestMessage(String messageManifest){

        String number = m_Text;

        try {
            SmsManager smsManager = SmsManager.getDefault();
            //smsManager.sendTextMessage(number, null, messageManifest, null, null);
            ArrayList<String> messageParts = smsManager.divideMessage(messageManifest);

            smsManager.sendMultipartTextMessage(number, null, messageParts, null, null);
            Toast.makeText(sContext, "Message Sent",
                    Toast.LENGTH_LONG).show();
        } catch (Exception ex) {
            Toast.makeText(sContext,ex.getMessage(),
                    Toast.LENGTH_LONG).show();
            ex.printStackTrace();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {

            case SMS_PERMISSION_CODE:{
                //If permission is not granted
                if(grantResults.length >0 && grantResults[0] != PackageManager.PERMISSION_GRANTED){
                    Toast.makeText(this,"Oops you just denied the permission",Toast.LENGTH_LONG).show();
                }
                return;
            }

        }
    }

    private void requestSmsPermission(){
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.SEND_SMS) != PackageManager.PERMISSION_GRANTED ){
            ActivityCompat.requestPermissions(this,new String[]{Manifest.permission.SEND_SMS},SMS_PERMISSION_CODE);
        }

    }

    public void setLogo(){

        SharedPreferences sharedPrefs = PreferenceManager.getDefaultSharedPreferences(this);
        String logo_selected = sharedPrefs.getString(constants.logo_check, "CodeLand");
        ImageView imageView = (ImageView)findViewById(R.id.imageViewBack);

        switch (logo_selected){
            case constants.CodeLand:
                imageView.setImageResource(R.drawable.back_codeland);
                break;
            case constants.Medicare:
                imageView.setImageResource(R.drawable.back_medicare);
                break;
            case constants.Maridi:
                imageView.setImageResource(R.drawable.back_maridi);
                break;
            case constants.AECS:
                imageView.setImageResource(R.drawable.back_codeland);
                break;
            default:
                imageView.setImageResource(R.drawable.back_codeland);
                break;

        }
    }


    public void startDeviceListActivity(View view) {
        stopConnection();
        Intent serverIntent = new Intent(this, activitydevicelist.class);
        startActivityForResult(serverIntent, REQUEST_CONNECT_DEVICE);
    }

    void setDeviceName(String deviceName) {
        this.deviceName = deviceName;
        this.bluetoothstatus.setText(getString(R.string.device_connected_msg_visited, deviceName));
    }

    private void setupConnector(BluetoothDevice connectedDevice) {
        try {
            String emptyName = getString(R.string.empty_device_name);
            weighingscale printerData = new weighingscale(connectedDevice, emptyName);
            connector = new devicebluetoothconnector(printerData, mHandler);
            connector.connect();
            setDeviceName(connectedDevice.getName());
        } catch (IllegalArgumentException e) {
            Log.i("devicecontrol", "setupConnector failed: " + e.getMessage());
        }
    }

    public static void askUserToPair() {
        Toast.makeText(sContext, "Printer not Selected | Select Bluetooth printer...", Toast.LENGTH_SHORT).show();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {

            case REQUEST_CONNECT_DEVICE:
                // When DeviceListActivity returns with a device to connect
                if (resultCode == Activity.RESULT_OK) {
                    String address = data.getStringExtra(activitydevicelist.EXTRA_DEVICE_ADDRESS);
                    BluetoothDevice device = btAdapter.getRemoteDevice(address);
                    bluetoothDevicePrinter = btAdapter.getRemoteDevice(address);
                    if (super.isAdapterReady() && (connector == null)) setupConnector(device);
                }
                break;

            case REQUEST_ENABLE_BT:
                // When the request to enable Bluetooth returns
                super.pendingRequestEnableBt = false;
                if (resultCode != Activity.RESULT_OK) {
                    Log.i("devicecontrol", "BT not enabled");
                }
                break;

        }
    }

    private static class printAsyncTask extends AsyncTask<Void, Void, Void>{

        @Override
        protected Void doInBackground(Void... params) {
            findBT();
            //openBT();
            //sendData(manifestString);
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            autoresponsestatus.setText(messageToDisplay);
            try {
                if (mmDevice != null) {
                    bluetoothstatus.setText(mmDevice.getName());
                }
            }catch (Exception e){
                e.printStackTrace();
            }
        }
    }

    @Override
    protected void onStop() {
        try {
            closeBT();
        } catch (IOException e) {
            e.printStackTrace();
        }
        super.onStop();
    }
}
