package com.cabs.softwarescanner.ui;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.bluetooth.BluetoothDevice;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.preference.PreferenceManager;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.cabs.softwarescanner.R;
import com.cabs.softwarescanner.bluetooth.activitydevicelist;
import com.cabs.softwarescanner.bluetooth.baseactivity;
import com.cabs.softwarescanner.constants;
import com.cabs.softwarescanner.customer;
import com.cabs.softwarescanner.database.customerdb;
import com.cabs.softwarescanner.utils.utility;
import com.cabs.softwarescanner.weighingscale;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.ref.WeakReference;
import java.util.Calendar;

import static com.cabs.softwarescanner.ui.activityscan.PERMISSIONS_REQUEST_CAMERA;

public class activitycollector extends baseactivity {

    //Adding variables for bluetooth connection
    //and capturing weight through bluetooth device
    static final int REQUEST_CONNECT_DEVICE = 3;
    static final int REQUEST_ENABLE_BT = 4;
    static final int NO_OF_BAG_ROWS = 7;
    private String deviceName;
    private TextView bluetoothstatus;
    private TextView autoweightstatus;
    private static devicebluetoothconnector connector;
    private static devicebluetoothconnector backUpConnector;  //Backup connector for resuming bluetooth-connection
    private String BackUpdeviceName;
    private static BluetoothResponseHandler mHandler;
    private static Double weight = 0.0;
    private Boolean canaddWeight = true;

    customer mCustomer;
    String base64String = "";
    public static final int SIGNATURE_ACTIVITY = 1;
    public static final int SCAN_ACTIVITY = 0;

    int visitDay;
    String Today;
    Context context;
    String mContents = "";

    Bags[] bagrows = new Bags[NO_OF_BAG_ROWS];

    EditText editTextToSetFocus;
    AlertDialog.Builder weightDialogBuilder;
    AlertDialog weightAlertDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_collector);
        context = this;

        setLogo();

        //...CODE FOR BLUETOOTH CONNECTIVITY...
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        //apply for camera permission
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                    Manifest.permission.CAMERA)) {
            } else {
                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.CAMERA}, PERMISSIONS_REQUEST_CAMERA);
            }
        }

        if (mHandler == null) mHandler = new activitycollector.BluetoothResponseHandler(this);
        else mHandler.setTarget(this);

        bluetoothstatus = (TextView) findViewById(R.id.bluetoothstatus);
        autoweightstatus = (TextView) findViewById(R.id.autoweightstatus);

        //EditText for setting focus
        editTextToSetFocus = (EditText) findViewById(R.id.editText_toSetFocus);
        editTextToSetFocus.setFocusable(true);
        editTextToSetFocus.requestFocus();

        Today = constants.GetToday();
        visitDay = Calendar.getInstance().get(Calendar.DAY_OF_YEAR);
        try {
            mCustomer = (customer) getIntent().getSerializableExtra("customer");

            mCustomer.mdb = customerdb.getInstance(this);

            TextView collectionPointTxt = (TextView) findViewById(R.id.collectionpoint);
            collectionPointTxt.setText(mCustomer.mName);

            bagrows[0] = new Bags((EditText) findViewById(R.id.blue_count), (EditText) findViewById(R.id.blue_weight), 1, (ImageView) findViewById(R.id.subweightBlue), (ImageView) findViewById(R.id.addweightBlue));
            bagrows[1] = new Bags((EditText) findViewById(R.id.red_count), (EditText) findViewById(R.id.red_weight), 2, (ImageView) findViewById(R.id.subweightRed), (ImageView) findViewById(R.id.addweighRed));
            bagrows[2] = new Bags((EditText) findViewById(R.id.yellow_count), (EditText) findViewById(R.id.yellow_weight), 3, (ImageView) findViewById(R.id.subweightYellow), (ImageView) findViewById(R.id.addweightYellow));
            bagrows[3] = new Bags((EditText) findViewById(R.id.container_count), (EditText) findViewById(R.id.container_weight), 4, (ImageView) findViewById(R.id.subweightContainers), (ImageView) findViewById(R.id.addweightContainers));
            bagrows[4] = new Bags((EditText) findViewById(R.id.sharps_count), (EditText) findViewById(R.id.sharps_weight), 5, (ImageView) findViewById(R.id.subweightSharps), (ImageView) findViewById(R.id.addweightSharps));
            bagrows[5] = new Bags((EditText) findViewById(R.id.other_count), (EditText) findViewById(R.id.other_weight), 6, (ImageView) findViewById(R.id.subweightOthers), (ImageView) findViewById(R.id.addweightOthers));
            bagrows[6] = new Bags((EditText) findViewById(R.id.whites_count), (EditText) findViewById(R.id.whites_weight), 7, (ImageView) findViewById(R.id.subweightWhitess), (ImageView) findViewById(R.id.addweightWhites));

            SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(this);
            boolean isContainerChecked = Boolean.parseBoolean(String.valueOf(pref.getBoolean(constants.container_check, false)));
            boolean isSharpsChecked = Boolean.parseBoolean(String.valueOf(pref.getBoolean(constants.sharps_check, false)));
            boolean isOthersChecked = Boolean.parseBoolean(String.valueOf(pref.getBoolean(constants.others_check, false)));

            if ( !isContainerChecked){

                LinearLayout linearLayoutContainer = (LinearLayout) findViewById(R.id.linearLayoutContainer);
                ImageView imageViewContainer = (ImageView) findViewById(R.id.imageViewcontainer);
                linearLayoutContainer.setVisibility(View.GONE);
                imageViewContainer.setVisibility(View.GONE);
            }

            if ( !isSharpsChecked){

                LinearLayout linearLayoutSharps = (LinearLayout) findViewById(R.id.linearLayoutSharp);
                ImageView imageViewSharps = (ImageView) findViewById(R.id.imageViewSharp);
                linearLayoutSharps.setVisibility(View.GONE);
                imageViewSharps.setVisibility(View.GONE);
            }

            if ( !isOthersChecked){

                LinearLayout linearLayoutOthers = (LinearLayout) findViewById(R.id.linearLayoutOther);
                ImageView imageViewOthers = (ImageView) findViewById(R.id.imageViewOther);
                linearLayoutOthers.setVisibility(View.GONE);
                imageViewOthers.setVisibility(View.GONE);
            }

        } catch (Exception e) {
            e.printStackTrace();
            Intent routeIntent = new Intent(getApplicationContext(), activityroutemaster.class);
            routeIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(routeIntent);
        }

        if (isConnected()) {
            setDeviceName(connector.getString());
        }

        //method for receiving data from edit texts through manual entry for all rows
        for (int i = 0; i < bagrows.length; i++) {
            final int row = i;
            bagrows[i].getData_count().setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    return false;
                }
            });

            bagrows[i].getData_count().setOnFocusChangeListener(new View.OnFocusChangeListener() {
                @Override
                public void onFocusChange(View v, boolean hasFocus) {
                    int cnt = 0;
                    if (bagrows[row].getData_count().getText().toString().equals("")) {
                        cnt = 0;
                    } else {
                        try {
                            cnt = Integer.parseInt(bagrows[row].getData_count().getText().toString());
                            if (cnt > 1200){
                                //bagrows[row].setManuallyEnteredCount(cnt);
                            //}else {
                                Toast.makeText(activitycollector.this, "Count value is more than 1200!", Toast.LENGTH_SHORT).show();
                                cnt = 0;
                                //bagrows[row].setManuallyEnteredCount(cnt);
                            }
                        }catch (Exception e){
                            Toast.makeText(activitycollector.this, "Please enter valid count value!", Toast.LENGTH_SHORT).show();
                            cnt = 0;
                            //bagrows[row].setManuallyEnteredCount(cnt);
                        }
                    }
                    bagrows[row].setManuallyEnteredCount(cnt);
                }
            });

            bagrows[i].getData_weight().setOnFocusChangeListener(new View.OnFocusChangeListener() {
                @Override
                public void onFocusChange(View v, boolean hasFocus) {
                    double wt = 0;
                    if (bagrows[row].getData_weight().getText().toString().equals("")) {
                        wt = 0;
                    } else {
                        try {
                            wt = Double.parseDouble(bagrows[row].getData_weight().getText().toString());
                        }catch (Exception e){
                            Toast.makeText(activitycollector.this, "Please enter valid weight value!", Toast.LENGTH_SHORT).show();
                            wt = 0;
                            //bagrows[row].showCount();
                        }
                    }
                    SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(activitycollector.this);
                    weightDialogBuilder = new AlertDialog.Builder(activitycollector.this);
                    weightAlertDialog = weightDialogBuilder.create();

                    Animation anim = new AlphaAnimation(0.0f, 1.0f);
                    anim.setDuration(500); //You can manage the blinking time with this parameter
                    anim.setStartOffset(20);
                    anim.setRepeatCount(10);

                    if (bagrows[row].getManuallyEnteredCount() == 0){
                        bagrows[row].getData_weight().startAnimation(anim);
                        bagrows[row].getData_weight().setText("0.0");
                        bagrows[row].setManuallyEnteredWeight(wt);
                        editTextToSetFocus.setFocusable(true);
                        editTextToSetFocus.requestFocus();
                        weightAlertShow(3);

                    }else if (wt < 0.0){
                        bagrows[row].getData_weight().startAnimation(anim);
                        bagrows[row].getData_weight().setText("0.0");
                        bagrows[row].getData_weight().clearFocus();
                        bagrows[row].setManuallyEnteredWeight(wt);
                        weightAlertShow(1);

                    }else if ( (wt/bagrows[row].getManuallyEnteredCount()) > Double.parseDouble(pref.getString(constants.max_weight, "35"))){
                        bagrows[row].getData_weight().startAnimation(anim);
                        bagrows[row].getData_weight().setText("0.0");
                        bagrows[row].getData_weight().clearFocus();
                        bagrows[row].setManuallyEnteredWeight(wt);
                        weightAlertShow(2);

                    }else if ((wt >= 0.0 && wt <= Double.parseDouble(pref.getString(constants.max_weight, "35"))) ){
                        bagrows[row].setManuallyEnteredWeight(wt);
                        //bagrows[row].showCount();
                    }

                }

            });
        }
    }

    //methods for plus button clicks
    public void addButtonClick(View view) {

        SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(activitycollector.this);
        if (weight < 0.0){
            weightDialogBuilder = new AlertDialog.Builder(this);
            weightAlertDialog = weightDialogBuilder.create();
            utility.showWeightAlert(constants.negativecheck, this, weightAlertDialog, null);

        }else if (weight > Double.parseDouble(pref.getString(constants.max_weight, "35"))){
            weightDialogBuilder = new AlertDialog.Builder(this);
            weightAlertDialog = weightDialogBuilder.create();
            utility.showWeightAlert(constants.maxweightcheck, this, weightAlertDialog, null);

        }else if ((weight >= 0.0 && weight <= Double.parseDouble(pref.getString(constants.max_weight, "35"))) /* && canaddWeight */){
            final int row = Integer.parseInt(view.getTag().toString());
            bagrows[row].addManualBagDetails(weight);
            canaddWeight = false;
            bagrows[row].showCount();
        }
    }


    public void subtractButtonClick(View view) {

        editTextToSetFocus.setFocusable(true);
        editTextToSetFocus.requestFocus();
        int row = Integer.parseInt(view.getTag().toString());
        bagrows[row].removeManualBagDetails();
        bagrows[row].showCount();
    }

    private boolean isConnected() {
        return (connector != null) && (connector.getState() == devicebluetoothconnector.STATE_CONNECTED);
    }

    private void setupConnector(BluetoothDevice connectedDevice) {
        stopConnection();
        try {
            String emptyName = getString(R.string.empty_device_name);
            weighingscale data = new weighingscale(connectedDevice, emptyName);
            connector = new devicebluetoothconnector(data, mHandler);
            connector.connect();
            //setting backup connector
            backUpConnector = new devicebluetoothconnector(data, mHandler);
            setDeviceName(connectedDevice.getName());
        } catch (IllegalArgumentException e) {
            Log.i("devicecontrol", "setupConnector failed: " + e.getMessage());
        }
    }

    void setDeviceName(String deviceName) {
        this.deviceName = deviceName;
        this.bluetoothstatus.setText(getString(R.string.device_connected_msg, deviceName));
    }

    public void onStart() {
        super.onStart();
    }

    public void Scanbarcodes(View view) {
        try {
            Intent intent = new Intent(this, activityscan.class);
            intent.putExtra("customer_id", mCustomer.mId);
            //before going to next activity stop bluetooth connection so connection on next activity can be established
            stopConnection();
            startActivityForResult(intent, SCAN_ACTIVITY);
        } catch (ActivityNotFoundException anfe) {
            anfe.printStackTrace();
        }
    }

    public void UpdateInputs() {

        for (Bags row : bagrows) {
            row.showCount();
        }

    }

    public void UpdateData() {
        for (Bags row : bagrows) {
            row.updateData();
        }
    }


    public void ProcessWeights(View view) {

        JSONObject params = new JSONObject();
        SharedPreferences sharedPrefs = PreferenceManager.getDefaultSharedPreferences(this);
        try {

            editTextToSetFocus.setFocusable(true);
            editTextToSetFocus.requestFocus();

            if (weightAlertDialog == null || !weightAlertDialog.isShowing()) {

                UpdateInputs();
                UpdateData();

                JSONArray array = new JSONArray();
                double total_weight = 0.0;
                int total_count = 0;

                for (Bags row : bagrows) {
                    JSONObject object = new JSONObject();
                    object.put("category_id", row.category_id);
                    object.put("category_weight", row.bag_weight);
                    object.put("category_count", row.bag_count);
                    object.put("barcode", row.barcodes);
                    array.put(object);

                    total_weight += row.bag_weight;
                    total_count += row.bag_count;
                }

                JSONObject json = new JSONObject();
                json.put("partner_id", mCustomer.mId);
                json.put("visit_datetime", Today);
                json.put("total_weight", total_weight);
                json.put("total_count", total_count);
                json.put("vehicle_id", sharedPrefs.getString(constants.vehicle_id, "0"));
                json.put("categories", array); //bag details in an array of JSON data
                json.put("signature", base64String);
                json.put("lat", mCustomer.mLatitude);
                json.put("lon", mCustomer.mLongitude);
                json.put("mobile_data", mCustomer.mMobileData);
                params.put("params", json);

                mCustomer.mData = params.toString();
                if (mCustomer.setVisited(Today, visitDay)) {
                    processFinish();
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void startSignature(View view) {

        Intent newintent = new Intent(this, activitysignature.class);
        newintent.putExtra("customerid", mCustomer.mId);
        startActivityForResult(newintent, SIGNATURE_ACTIVITY);
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case SIGNATURE_ACTIVITY:
                if (resultCode == RESULT_OK) {

                    Bundle bundle = data.getExtras();
                    if (bundle.getString("status").equalsIgnoreCase("done")) {
                        Toast toast = Toast.makeText(this, "Signature capture successful!", Toast.LENGTH_SHORT);
                        toast.setGravity(Gravity.TOP, 105, 50);
                        toast.show();

                        base64String = bundle.getString("data");
                    }
                }
                break;
            case SCAN_ACTIVITY:
                try {
                    if (resultCode == RESULT_OK) {
                        Bundle bundle = data.getExtras();
                        mContents = bundle.getString("RESULT_STRING");
                        if (!mContents.isEmpty()) {
                            String[] tokens_unclean = mContents.split(",");
                            for (String token : tokens_unclean) {
                                try {
                                    bagrows[Character.getNumericValue(token.charAt(0)) - 1].addBarcode(token);
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }

                            UpdateInputs();
                            mContents = "";
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();

                    Toast toast = Toast.makeText(context, "Invalid Barcode", Toast.LENGTH_LONG);
                    toast.setGravity(Gravity.TOP, 25, 400);
                    toast.show();
                    mContents = "";
                }
                break;
            case REQUEST_CONNECT_DEVICE:
                // When DeviceListActivity returns with a device to connect
                if (resultCode == Activity.RESULT_OK) {
                    String address = data.getStringExtra(activitydevicelist.EXTRA_DEVICE_ADDRESS);
                    BluetoothDevice device = btAdapter.getRemoteDevice(address);
                    if (super.isAdapterReady() && (connector == null)) setupConnector(device);
                }
                break;
            case REQUEST_ENABLE_BT:
                // When the request to enable Bluetooth returns
                super.pendingRequestEnableBt = false;
                if (resultCode != Activity.RESULT_OK) {
                    Log.i("devicecontrol", "BT not enabled");
                }
                break;
        }
    }

    public void processFinish() {
        stopConnection();
        try {
            Toast toast = Toast.makeText(this, "Successfully submitted", Toast.LENGTH_LONG);
            toast.setGravity(Gravity.TOP, 25, 400);
            toast.show();
            finish();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onResume() {
        super.onResume();

        if (backUpConnector != null) {
            resumeConnection();
        }

        for (int i = 0; i < bagrows.length; i++) {
            bagrows[i].getData_weight().setTextColor(getResources().getColor(R.color.colorPrimary));
            bagrows[i].getData_count().setTextColor(getResources().getColor(R.color.colorPrimary));
        }
    }


    private void stopConnection() {
        if (connector != null) {
            backUpConnector = connector; //for resuming the connection
            BackUpdeviceName = deviceName;
            connector.stop();
            connector = null;
            deviceName = null;
        }
    }

    private void resumeConnection() {
        if (connector == null) {
            deviceName = BackUpdeviceName;
            connector = backUpConnector;
            connector.connect();
        }
    }

    public void startDeviceListActivity(View view) {
        stopConnection();
        Intent serverIntent = new Intent(this, activitydevicelist.class);
        startActivityForResult(serverIntent, REQUEST_CONNECT_DEVICE);
    }

    void setWeight(String weight) {
        double transientweight = Double.parseDouble(weight);

        if (transientweight == this.weight && !canaddWeight) {
            canaddWeight = false;
        }
        if (transientweight != this.weight) {
            canaddWeight = true;
        }

        this.weight = transientweight;
        this.autoweightstatus.setText(getString(R.string.device_weight_msg, this.weight));
    }

    private static class BluetoothResponseHandler extends Handler {
        private WeakReference<activitycollector> mActivity;
        static String data = "";

        BluetoothResponseHandler(activitycollector activity) {
            mActivity = new WeakReference<>(activity);
        }

        void setTarget(activitycollector target) {
            mActivity.clear();
            mActivity = new WeakReference<>(target);
        }

        @Override
        public void handleMessage(Message msg) {
            activitycollector activity = mActivity.get();

            if (activity != null) {
                switch (msg.what) {
                    case MESSAGE_STATE_CHANGE:

                        Log.i("devicecontrol", "MESSAGE_STATE_CHANGE: " + msg.arg1);
                        switch (msg.arg1) {
                            case devicebluetoothconnector.STATE_CONNECTED:
                                //Add data to the status bar
                                break;
                            case devicebluetoothconnector.STATE_CONNECTING:
                                break;
                            case devicebluetoothconnector.STATE_NONE:
                                //Add data to the status bar
                                break;
                        }
                        break;

                    case MESSAGE_READ:
                        try {
                            if (msg.arg1 == 1)
                                data = data.concat((String) msg.obj);
                            else if (msg.arg1 > 5) {
                                data = data.concat((String) msg.obj);
                                String[] weights = data.split("\r\n");
                                activity.setWeight(weights[0]);
                                data = "";
                            }
                        } catch (Exception e) {
                            data = "";
                            break;
                        }
                        break;
                    case MESSAGE_DEVICE_NAME:
                        activity.setDeviceName((String) msg.obj);
                        break;

                    case MESSAGE_WRITE:
                        // stub
                        break;

                    case MESSAGE_TOAST:
                        // stub
                        break;
                }
            }
        }
    }

    public void addBagPopUp(final View view){

        editTextToSetFocus.setFocusable(true);
        editTextToSetFocus.requestFocus();
        if ( !isConnected()){
            weight = 0.0;
            this.bluetoothstatus.setText("");
        }

        AlertDialog.Builder addBagAlertBuilder = new AlertDialog.Builder(this);
        AlertDialog addBagAlertDialog = addBagAlertBuilder.create();
        addBagAlertDialog.setTitle("Want to add 1 bag with "+ weight +" Kg");
        addBagAlertDialog.setButton(DialogInterface.BUTTON_POSITIVE, "YES", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                addButtonClick(view);
            }
        });

        addBagAlertDialog.setButton(DialogInterface.BUTTON_NEGATIVE, "NO", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        addBagAlertDialog.show();

    }

    public void setLogo(){

        SharedPreferences sharedPrefs = PreferenceManager.getDefaultSharedPreferences(this);
        String logo_selected = sharedPrefs.getString(constants.logo_check, "CodeLand");
        ImageView imageView = (ImageView)findViewById(R.id.imageViewBack);

        switch (logo_selected){
            case constants.CodeLand:
                imageView.setImageResource(R.drawable.back_codeland);
                break;
            case constants.Medicare:
                imageView.setImageResource(R.drawable.back_medicare);
                break;
            case constants.Maridi:
                imageView.setImageResource(R.drawable.back_maridi);
                break;
            case constants.AECS:
                imageView.setImageResource(R.drawable.back_codeland);
                break;
            default:
                imageView.setImageResource(R.drawable.back_codeland);
                break;

        }
    }

    public void weightAlertShow(int from) {


        if (weightAlertDialog != null && !weightAlertDialog.isShowing()) {

            if (from == 1){
                weightAlertDialog.setTitle("Negative weight from weighing scale!");
                weightAlertDialog.setMessage("Reset the scale to 0.0...");
            }else if (from == 2){
                SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(activitycollector.this);
                weightAlertDialog.setTitle("Weight for the bag is greater than "+ Double.parseDouble(pref.getString(constants.max_weight, "0.0"))+" KG");
                weightAlertDialog.setMessage("Check weight of the bag...");
            }else if (from == 3){
                weightAlertDialog.setTitle("Add count before adding weight!");
            }
            weightAlertDialog.setButton(DialogInterface.BUTTON_POSITIVE, "OK", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                }
            });
            weightAlertDialog.show();
        }
    }

    @Override
    public synchronized void onPause() {
        editTextToSetFocus.setFocusable(true);
        editTextToSetFocus.requestFocus();
        super.onPause();
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {

        boolean bTouch = super.onTouchEvent(event);
        if (!bTouch){
            editTextToSetFocus.setFocusable(true);
            editTextToSetFocus.requestFocus();
        }
        return bTouch;
    }
}
