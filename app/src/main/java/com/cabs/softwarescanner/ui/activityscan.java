package com.cabs.softwarescanner.ui;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.bluetooth.BluetoothDevice;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.hardware.Camera;
import android.media.AudioManager;
import android.media.ToneGenerator;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.os.Vibrator;
import android.preference.PreferenceManager;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.cabs.softwarescanner.R;
import com.cabs.softwarescanner.bluetooth.activitydevicelist;
import com.cabs.softwarescanner.bluetooth.baseactivity;
import com.cabs.softwarescanner.constants;
import com.cabs.softwarescanner.utils.utility;
import com.cabs.softwarescanner.weighingscale;
import com.dynamsoft.barcode.Barcode;
import com.dynamsoft.barcode.BarcodeReader;
import com.dynamsoft.barcode.FinishCallback;
import com.dynamsoft.barcode.ReadResult;

import java.lang.ref.WeakReference;
import java.util.List;
import java.util.concurrent.locks.ReentrantReadWriteLock;

public class activityscan extends baseactivity implements Camera.PreviewCallback {
    public static String TAG = "DBRDemo";
    private static final String DEVICE_NAME = "DEVICE_NAME";
    static final int REQUEST_CONNECT_DEVICE = 3;
    static final int REQUEST_ENABLE_BT = 4;
    private String deviceName;
    private TextView bluetoothstatus;
    private TextView autoweightstatus;
    private static devicebluetoothconnector connector;
    private static devicebluetoothconnector backUpConnector;
    private String BackUpdeviceName;
    private static BluetoothResponseHandler mHandler;
    private static Double weight = 0.0;
    private Boolean canaddWeight = true;
    private String contents = "";

    AlertDialog.Builder dialogBuilder;
    AlertDialog barcodeAlertDialog;

    AlertDialog.Builder weightDialogBuilder;
    AlertDialog weightAlertDialog;

    String customerId = "";

    SharedPreferences pref;

    static final int invalidCheck = 1;
    static final int customerIdCheck = 2;
    static final int container_sharpsCheck = 3;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_scan);

        setLogo();
        pref = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());

        Bundle b = getIntent().getExtras();
        customerId = String.valueOf(b.getInt("customer_id"));

        dialogBuilder = new AlertDialog.Builder(activityscan.this);
        barcodeAlertDialog = dialogBuilder.create();
        barcodeAlertDialog.setCanceledOnTouchOutside(false);
        barcodeAlertDialog.setTitle("Invalid Barcode");
        barcodeAlertDialog.setButton(DialogInterface.BUTTON_POSITIVE, "OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
            }
        });

        weightDialogBuilder = new AlertDialog.Builder(activityscan.this);
        weightAlertDialog = weightDialogBuilder.create();
        weightAlertDialog.setCanceledOnTouchOutside(false);
        weightAlertDialog.setButton(DialogInterface.BUTTON_POSITIVE, "OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
            }
        });


        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        contents ="";
        //apply for camera permission
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA) !=  PackageManager.PERMISSION_GRANTED) {
            if (!ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.CAMERA)) {
                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.CAMERA}, PERMISSIONS_REQUEST_CAMERA);
            }
        }

        if (mHandler == null) mHandler = new BluetoothResponseHandler(this);
        else mHandler.setTarget(this);

        bluetoothstatus = (TextView) findViewById(R.id.bluetoothstatus);
        autoweightstatus = (TextView) findViewById(R.id.autoweightstatus);
        mPreview = (FrameLayout) findViewById(R.id.camera_preview);

        //SDK Key for dynamsoft barcode reader
        mBarcodeReader = new BarcodeReader(constants.BARCODE_LICENSE);
        mFlashImageView = (ImageView)findViewById(R.id.ivFlash);
        mFlashTextView = (TextView)findViewById(R.id.tvFlash);
        mRectLayer = (RectLayer)findViewById(R.id.rectLayer);

        mSurfaceHolder = new CameraPreview(activityscan.this);
        mPreview.addView(mSurfaceHolder);

        if (isConnected()) {
            setDeviceName(connector.getString());
        }
    }

    static final int PERMISSIONS_REQUEST_CAMERA = 101;
    private FrameLayout mPreview = null;
    private CameraPreview mSurfaceHolder = null;
    private Camera mCamera = null;
    private BarcodeReader mBarcodeReader;
    private long mBarcodeFormat = Barcode.OneD;
    private ImageView mFlashImageView;
    private TextView mFlashTextView;
    private RectLayer mRectLayer;
    private boolean mIsDialogShowing = false;
    private boolean mIsReleasing = false;
    final ReentrantReadWriteLock mRWLock = new ReentrantReadWriteLock();

    @Override
    public synchronized void onResume() {
        super.onResume();
        resumeCamera();
        if (backUpConnector != null){
            resumeConnection();
        }
    }

    public void resumeCamera() {

        waitForRelease();
        if (mCamera == null) {
            openCamera();
        }else {
            mCamera.startPreview();
            mCamera.setPreviewCallback(activityscan.this);
        }
    }

    @Override
    public synchronized void onPause() {
        super.onPause();
        pauseCamera();
    }

    public void pauseCamera() {
        if (mCamera != null) {
            mCamera.stopPreview();
            mCamera.setPreviewCallback(null);
            mSurfaceHolder.getHolder().removeCallback(mSurfaceHolder);
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (mCamera != null) {
            mSurfaceHolder.stopPreview();
            mCamera.setPreviewCallback(null);
            mSurfaceHolder.getHolder().removeCallback(mSurfaceHolder);
            mIsReleasing = true;
            releaseCamera();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        waitForRelease();
        if (mCamera != null) {
            mCamera.stopPreview();
            mCamera.setPreviewCallback(null);
            mSurfaceHolder.getHolder().removeCallback(mSurfaceHolder);
            mCamera.release();
            mCamera = null;
        }
    }

    @Override
    public void finish(){
        super.finish();
        Intent intent=new Intent();
        intent.putExtra("RESULT_STRING", contents);
        setResult(RESULT_OK, intent);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString(DEVICE_NAME, deviceName);
    }
    private boolean isConnected() {
        return (connector != null) && (connector.getState() == devicebluetoothconnector.STATE_CONNECTED);
    }

    private void stopConnection() {
        if (connector != null) {
            backUpConnector = connector; //added later
            connector.stop();
            connector = null;
            BackUpdeviceName = deviceName;
            deviceName = null;
        }
    }

    private void resumeConnection(){
        if (connector == null){
            deviceName = BackUpdeviceName;
            connector = backUpConnector;
            connector.connect();
        }

    }

    public void startDeviceListActivity(View view) {
        stopConnection();
        Intent serverIntent = new Intent(this, activitydevicelist.class);
        startActivityForResult(serverIntent, REQUEST_CONNECT_DEVICE);
    }

    public void setFlash(View v) {
        if (mCamera != null) {

            try {
                Camera.Parameters p = mCamera.getParameters();
                String flashMode = p.getFlashMode();
                if (flashMode.equals(Camera.Parameters.FLASH_MODE_OFF)) {
                    p.setFlashMode(Camera.Parameters.FLASH_MODE_TORCH);
                    mFlashImageView.setImageResource(R.mipmap.flash_on);
                } else {
                    p.setFlashMode(Camera.Parameters.FLASH_MODE_OFF);
                    mFlashImageView.setImageResource(R.mipmap.flash_off);
                }
                mCamera.setParameters(p);
                mCamera.startPreview();
            } catch (Exception e) {
                Toast.makeText(this, "Flash not available!", Toast.LENGTH_SHORT).show();
            }
        }
    }

    @Override
    public void onBackPressed() {
    }

    public void setComplete(View v) {
        Intent intent=new Intent();
        intent.putExtra("RESULT_STRING", contents);
        setResult(RESULT_OK, intent);
        //bluetooth connection stops
        stopConnection();
        finish();
    }

    private static Camera getCameraInstance(){
        Camera c = null;
        try {
            c = Camera.open(); // attempt to get a Camera instance
        }
        catch (Exception e){
            Log.i(TAG, "Camera is not available (in use or does not exist)");
        }
        return c; // returns null if camera is unavailable
    }

    private void openCamera()
    {
        new Thread(new Runnable() {
            @Override
            public void run() {
                mCamera = getCameraInstance();
                if (mCamera != null) {

                    mCamera.setPreviewCallback(activityscan.this);
                    mCamera.setDisplayOrientation(90);
                    Camera.Parameters cameraParameters = mCamera.getParameters();

                    List<String> supportedFocusModes = cameraParameters.getSupportedFocusModes();

                    if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.ICE_CREAM_SANDWICH &&
                            supportedFocusModes.contains(Camera.Parameters.FOCUS_MODE_CONTINUOUS_VIDEO)) {
                        cameraParameters.setFocusMode(Camera.Parameters.FOCUS_MODE_CONTINUOUS_VIDEO);
                    }
                    else if (supportedFocusModes.contains(Camera.Parameters.FOCUS_MODE_CONTINUOUS_PICTURE)) {
                        cameraParameters.setFocusMode(Camera.Parameters.FOCUS_MODE_CONTINUOUS_PICTURE);
                    }
                    else if (supportedFocusModes.contains(Camera.Parameters.FOCUS_MODE_MACRO)) {
                        cameraParameters.setFocusMode(Camera.Parameters.FOCUS_MODE_MACRO);
                    }
                    else if (supportedFocusModes.contains(Camera.Parameters.FOCUS_MODE_AUTO)) {
                        // auto focus on request only
                        cameraParameters.setFocusMode(Camera.Parameters.FOCUS_MODE_AUTO);
                    }
                    mCamera.setParameters(cameraParameters);
                }


                Message message = handler.obtainMessage(OPEN_CAMERA, 1);
                message.sendToTarget();
            }
        }).start();
    }

    private void releaseCamera()
    {
        new Thread(new Runnable() {
            @Override
            public void run() {
                mCamera.release();
                mCamera = null;
                mRWLock.writeLock().lock();
                mIsReleasing = false;
                mRWLock.writeLock().unlock();
            }
        }).start();
    }

    private void waitForRelease() {
        while (true) {
            mRWLock.readLock().lock();
            if (mIsReleasing) {
                mRWLock.readLock().unlock();
                try {
                    Thread.sleep(10);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            } else {
                mRWLock.readLock().unlock();
                break;
            }
        }
    }

    private boolean mFinished = true;
    private final static int READ_RESULT = 1;
    private final static int OPEN_CAMERA = 2;
    private final static int RELEASE_CAMERA = 3;

    Handler handler = new Handler(Looper.getMainLooper()) {
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case READ_RESULT:

                    ReadResult result = (ReadResult)msg.obj;
                    Barcode barcode = result.barcodes == null ? null : result.barcodes[0];
                    if (barcode != null) {

                        String barcodeStrByScanner = result.barcodes[0].displayValue;
                        String bagIdStr = null;
                        if(barcodeStrByScanner != null && !barcodeStrByScanner.isEmpty()) {
                            bagIdStr = barcodeStrByScanner.substring(0, 1);
                        }
                        try {
                            int bagId = Integer.parseInt(bagIdStr);
                            int barcodeSizeByUser = Integer.parseInt(pref.getString(constants.barcode_size, "0"));
                            boolean isCustomerIdToBeChecked = Boolean.parseBoolean(String.valueOf(pref.getBoolean(constants.customer_Id_check, false)));
                            boolean isContainerToBeChecked = Boolean.parseBoolean(String.valueOf(pref.getBoolean(constants.container_check, false)));
                            boolean isSharpsToBeChecked = Boolean.parseBoolean(String.valueOf(pref.getBoolean(constants.sharps_check, false)));
                            boolean isOthersIdToBeChecked = Boolean.parseBoolean(String.valueOf(pref.getBoolean(constants.others_check, false)));

                            if (result.barcodes[0].displayValue.length() >= barcodeSizeByUser && (bagId < 8 && bagId > 0)) {

                                if ( !isContainerToBeChecked && bagId == 4){
                                    showBarcodeAlert(container_sharpsCheck);

                                }else if ( !isSharpsToBeChecked && bagId == 5){
                                    showBarcodeAlert(container_sharpsCheck);

                                }else if ( !isOthersIdToBeChecked && bagId == 6){
                                    showBarcodeAlert(container_sharpsCheck);

                                }else if ( (!isCustomerIdToBeChecked) ||( barcodeStrByScanner.contains(customerId+"")) ){

                                    if (activityscan.weight < 0.0) {

                                        utility.showWeightAlert(constants.negativecheck, activityscan.this, weightAlertDialog, barcodeAlertDialog);
                                    } else if (activityscan.weight > Double.parseDouble(pref.getString(constants.max_weight, "35"))) {

                                        utility.showWeightAlert(constants.maxweightcheck, activityscan.this, weightAlertDialog, barcodeAlertDialog);
                                    }else if ( !barcodeAlertDialog.isShowing() && !weightAlertDialog.isShowing()){
                                        if (!isConnected() && activityscan.weight == 0.0) {
                                            activityscan.weight = 0.0;
                                            Toast.makeText(getApplicationContext(), "Weight is not captured / Bluetooth Not Connected", Toast.LENGTH_SHORT).show();
                                        }
                                        contents = contents.concat(result.barcodes[0].displayValue + ":" + Double.toString(activityscan.weight) + ",");
                                        ToneGenerator toneGen1 = new ToneGenerator(AudioManager.STREAM_MUSIC, 100);
                                        toneGen1.startTone(ToneGenerator.TONE_CDMA_PIP, 150);
                                        ((Vibrator) getSystemService(VIBRATOR_SERVICE)).vibrate(250);
                                    }
                                }
                                else {
                                    showBarcodeAlert(customerIdCheck);
                                }

                            } else {
                                showBarcodeAlert(invalidCheck);
                            }
                        } catch (Exception e){
                            showBarcodeAlert(invalidCheck);
                        }

                    } else {
                        if (result.errorCode != BarcodeReader.DBR_OK)
                            Log.i(TAG, "Error:" + result.errorString);
                    }
                    mFinished = true;

                    break;
                case OPEN_CAMERA:
                    if (mCamera != null) {

                        try {
                            mCamera.setPreviewCallback(activityscan.this);
                            mSurfaceHolder.setCamera(mCamera);
                            Camera.Parameters p = mCamera.getParameters();
                            if (mFlashTextView.getText().equals("Flash on"))
                                p.setFlashMode(Camera.Parameters.FLASH_MODE_TORCH);
                            mCamera.setParameters(p);
                            mSurfaceHolder.startPreview();

                        }catch (Exception e){
                            e.printStackTrace();
                        }
                    }
                    break;
                case RELEASE_CAMERA:

                    break;
            }
        }
    };

    @Override
    public void onPreviewFrame(byte[] data, Camera camera) {
        if (mFinished && !mIsDialogShowing) {
            mFinished = false;
            Camera.Size size = camera.getParameters().getPreviewSize();
            mBarcodeReader.readSingleAsync(data, size.width, size.height, mBarcodeFormat, new FinishCallback() {
                @Override
                public void onFinish(ReadResult readResult) {
                    Message message = handler.obtainMessage(READ_RESULT, readResult);
                    message.sendToTarget();
                }
            });
        }
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case REQUEST_CONNECT_DEVICE:
                // When DeviceListActivity returns with a device to connect
                if (resultCode == Activity.RESULT_OK) {
                    String address = data.getStringExtra(activitydevicelist.EXTRA_DEVICE_ADDRESS);
                    BluetoothDevice device = btAdapter.getRemoteDevice(address);
                    if (super.isAdapterReady() && (connector == null)) setupConnector(device);
                }
                break;
            case REQUEST_ENABLE_BT:
                // When the request to enable Bluetooth returns
                super.pendingRequestEnableBt = false;
                if (resultCode != Activity.RESULT_OK) {
                    Log.i("devicecontrol", "BT not enabled");
                }
                break;
        }
    }

    private void setupConnector(BluetoothDevice connectedDevice) {
        stopConnection();
        try {
            String emptyName = getString(R.string.empty_device_name);
            weighingscale data = new weighingscale(connectedDevice, emptyName);
            connector = new devicebluetoothconnector(data, mHandler);
            connector.connect();
            //setting backup connector
            backUpConnector = new devicebluetoothconnector(data, mHandler);

            setDeviceName(connectedDevice.getName());
        } catch (IllegalArgumentException e) {
            Log.i("devicecontrol", "setupConnector failed: " + e.getMessage());
        }
    }

    void setDeviceName(String deviceName) {
        this.deviceName = deviceName;
        //setting back up device name
        this.BackUpdeviceName = deviceName;
        this.bluetoothstatus.setText(getString(R.string.device_connected_msg, deviceName));
    }

    void setWeight(String weight) {
        double transientweight = Double.parseDouble(weight);

        if ( transientweight == this.weight && !canaddWeight) {
            canaddWeight = false;
        }
        if ( transientweight != this.weight ) {
            canaddWeight = true;
        }

        this.weight = transientweight;
        this.autoweightstatus.setText(getString(R.string.device_weight_msg, this.weight));
    }

    private static class BluetoothResponseHandler extends Handler {
        private WeakReference<activityscan> mActivity;
        static String data = "";

        BluetoothResponseHandler(activityscan activity) {
            mActivity = new WeakReference<>(activity);
        }

        void setTarget(activityscan target) {
            mActivity.clear();
            mActivity = new WeakReference<>(target);
        }

        @Override
        public void handleMessage(Message msg) {
            activityscan activity = mActivity.get();

            if (activity != null) {
                switch (msg.what) {
                    case MESSAGE_STATE_CHANGE:

                        Log.i("devicecontrol", "MESSAGE_STATE_CHANGE: " + msg.arg1);
                        switch (msg.arg1) {
                            case devicebluetoothconnector.STATE_CONNECTED:
                                //Add data to the status bar
                                break;
                            case devicebluetoothconnector.STATE_CONNECTING:
                                break;
                            case devicebluetoothconnector.STATE_NONE:
                                //Add data to the status bar
                                break;
                        }
                        break;

                    case MESSAGE_READ:
                        try {
                            if(msg.arg1 == 1)
                                data = data.concat((String)msg.obj);
                            else if(msg.arg1 > 5) {
                                data = data.concat((String) msg.obj);
                                String[] weights = data.split("\r\n");
                                activity.setWeight(weights[0]);
                                data = "";
                            }
                        }catch (Exception e){
                            data = "";
                            break;
                        }
                        break;
                    case MESSAGE_DEVICE_NAME:
                        activity.setDeviceName((String) msg.obj);
                        break;

                    case MESSAGE_WRITE:
                        // stub
                        break;

                    case MESSAGE_TOAST:
                        // stub
                        break;
                }
            }
        }
    }

    public void showBarcodeAlert(int from) {

        if( !barcodeAlertDialog.isShowing() && !weightAlertDialog.isShowing()) {

            switch (from){
                case invalidCheck:
                    barcodeAlertDialog.setMessage("Scan Again...");
                    break;
                case customerIdCheck:
                    barcodeAlertDialog.setMessage("Customer ID is different...");
                    break;
                case container_sharpsCheck:
                    barcodeAlertDialog.setMessage("");
                    break;
            }
            barcodeAlertDialog.show();
        }
    }

    public void setLogo(){

        SharedPreferences sharedPrefs = PreferenceManager.getDefaultSharedPreferences(this);
        String logo_selected = sharedPrefs.getString(constants.logo_check, "CodeLand");
        ImageView imageView = (ImageView)findViewById(R.id.imageViewBack);

        switch (logo_selected){
            case constants.CodeLand:
                imageView.setImageResource(R.drawable.back_codeland);
                break;
            case constants.Medicare:
                imageView.setImageResource(R.drawable.back_medicare);
                break;
            case constants.Maridi:
                imageView.setImageResource(R.drawable.back_maridi);
                break;
            case constants.AECS:
                imageView.setImageResource(R.drawable.back_codeland);
                break;
            default:
                imageView.setImageResource(R.drawable.back_codeland);
                break;

        }
    }
}
