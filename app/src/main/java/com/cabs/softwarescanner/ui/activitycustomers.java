package com.cabs.softwarescanner.ui;

import android.app.Activity;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.text.Editable;
import android.text.TextWatcher;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.cabs.softwarescanner.R;
import com.cabs.softwarescanner.constants;
import com.cabs.softwarescanner.customer;
import com.cabs.softwarescanner.database.customerdb;
import com.cabs.softwarescanner.utils.adaptercustomer;

import java.util.ArrayList;

public class activitycustomers extends Activity {

    adaptercustomer adbCustomer;
    customerdb mdb;
    TextView textViewToShowCustomerCount;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_customer);

        setLogo();

        EditText inputSearch = (EditText)findViewById(R.id.customerSearch);
        ListView listView = (ListView)findViewById(R.id.customer_list_view);

        adbCustomer = new adaptercustomer(this, R.id.customer_name, getCustomers());
        listView.setAdapter(adbCustomer);

        textViewToShowCustomerCount = (TextView) findViewById(R.id.textViewToShowCustomerCount);
        textViewToShowCustomerCount.setText(getCustomers().size() + "");

        inputSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
                activitycustomers.this.adbCustomer.getFilter().filter(charSequence);
            }

            @Override
            public void afterTextChanged(Editable editable) {
            }
        });
    }

    private ArrayList<customer> getCustomers(){
        Cursor c =null;
        ArrayList<customer> retVal = null;

        try {
            mdb = customerdb.getInstance(this);
            c = mdb.getCustomers(false);
            retVal = new ArrayList<customer>();

            while (c.moveToNext()) {
                customer custobj = new customer();
                custobj.mId = c.getInt(0);
                custobj.mName = c.getString(1);
                custobj.mData = c.getString(2);
                retVal.add(custobj);
            }
        }catch(Exception e) {
            e.printStackTrace();
        }finally {
           if(c !=null & !c.isClosed())
               c.close();
        }

        return retVal;
    }

    @Override
    protected void onDestroy(){
        if( mdb != null)
            mdb.close();

        super.onDestroy();
    }

    public void setLogo(){

        SharedPreferences sharedPrefs = PreferenceManager.getDefaultSharedPreferences(this);
        String logo_selected = sharedPrefs.getString(constants.logo_check, "CodeLand");
        ImageView imageView = (ImageView)findViewById(R.id.imageViewBack);

        switch (logo_selected){
            case constants.CodeLand:
                imageView.setImageResource(R.drawable.back_codeland);
                break;
            case constants.Medicare:
                imageView.setImageResource(R.drawable.back_medicare);
                break;
            case constants.Maridi:
                imageView.setImageResource(R.drawable.back_maridi);
                break;
            case constants.AECS:
                imageView.setImageResource(R.drawable.back_codeland);
                break;
            default:
                imageView.setImageResource(R.drawable.back_codeland);
                break;

        }
    }
}
