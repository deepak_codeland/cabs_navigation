package com.cabs.softwarescanner.ui;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.Environment;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.cabs.softwarescanner.R;
import com.cabs.softwarescanner.constants;
import com.cabs.softwarescanner.interfaces.IAsyncResponse;
import com.cabs.softwarescanner.utils.utility;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;

public class activitysplash extends Activity implements IAsyncResponse {

    private static final String mTag = activitysplash.class.getSimpleName();
    private static final int RESULT_SETTINGS = 1; //Result Settings is a placeholder variable
    private static final int EXTERNAL_PERMISSION_CODE = 25;
    private static final int READ_PERMISSION_CODE = 24;
    private final int SPLASH_DISPLAY_LENGTH = 3000;

    EditText editTextUserName;
    EditText editTextPassword;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, EXTERNAL_PERMISSION_CODE);
        }
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, READ_PERMISSION_CODE);
        }

        if ( isExternalStorageWritable() ) {

            File appDirectory = new File( Environment.getExternalStorageDirectory() + "/CollectionAutomationLog" );
            File logDirectory = new File( appDirectory + "/log" );
            File logFile = new File( logDirectory, "logcat" + ".txt" );

            // create app folder
            if ( !appDirectory.exists() ) {
                appDirectory.mkdir();
            }

            // create log folder
            if ( !logDirectory.exists() ) {
                logDirectory.mkdir();
            }

            // clear the previous logcat and then write the new one to the file
            if ( logFile.exists()){
                logFile.delete();
            }

            try {
                Process process = Runtime.getRuntime().exec("logcat -c");
                process = Runtime.getRuntime().exec("logcat -f " + logFile);
            } catch ( IOException e ) {
                e.printStackTrace();
            }

        } else if ( isExternalStorageReadable() ) {
            // only readable
        } else {
            // not accessible
        }


        setContentView(R.layout.activity_splash);

        Animation FadeinAnimation;
        FadeinAnimation = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.fade_in);
        ImageView imageView = (ImageView)findViewById(R.id.splash_logo);
        setLogo();
        imageView.startAnimation(FadeinAnimation);

        editTextUserName = (EditText) findViewById(R.id.editTextUserName);
        editTextPassword = (EditText) findViewById(R.id.editTextPassword);
        SharedPreferences sharedPrefs = PreferenceManager.getDefaultSharedPreferences(this);
        if (sharedPrefs.getString(constants.username, "").contentEquals("")){
            editTextUserName.setHint("Enter user name");
        }else {
            editTextUserName.setText(sharedPrefs.getString(constants.username, ""));
        }

        if (sharedPrefs.getString(constants.password, "").contentEquals("")){
            editTextPassword.setHint("Enter password");
        }else {
            editTextPassword.setText(sharedPrefs.getString(constants.password, ""));
        }


        utility.checkPlayServices(getApplicationContext(), this);
    }

    //This method will be called when the user will tap on allow or deny
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {

        //Checking the request code of our request
        if(requestCode == EXTERNAL_PERMISSION_CODE){

            //If permission is granted
            if(grantResults.length >0 && grantResults[0] == PackageManager.PERMISSION_GRANTED){
                //MoveToRouteMaster();
            }else{
                //Displaying another toast if permission is not granted
                Toast.makeText(this,"Oops you just denied the permission",Toast.LENGTH_LONG).show();
            }
        }
    }

    @Override
    public void processFinish(JSONObject jsonObject) {

        try {
            JSONObject json = jsonObject.optJSONObject("result");
            String stringCompanyId = json.optString("company_id");

            if ( !stringCompanyId.contentEquals("null")) {
                SharedPreferences pref = getSharedPreferences(constants.settings_filename, MODE_PRIVATE);

                if (utility.saveSession(pref, json.getString(constants.session_id), json.optLong(constants.session_expiry))) {
                    MoveToRouteMaster();
                }
            }else {
                Toast.makeText(this, "Wrong username or password", Toast.LENGTH_SHORT).show();
            }
        }catch(JSONException e){
            e.printStackTrace();
            Log.v(mTag, "Bad json response");
            Toast.makeText(this, "Failed to login !", Toast.LENGTH_SHORT).show();
        }catch(NullPointerException e){
            e.printStackTrace();
            Log.v(mTag, "Failed to login");
            Toast.makeText(this, "Failed to login !", Toast.LENGTH_SHORT).show();

        }
    }


    public void startSettings(View view){
        Intent i = new Intent(this, activitysettings.class);
        startActivityForResult(i, RESULT_SETTINGS);
    }

    public void startLogin(View view){

        SharedPreferences sharedPrefs = PreferenceManager.getDefaultSharedPreferences(this);
        String vehicle_id = sharedPrefs.getString(constants.vehicle_id, "0");

        if ( (editTextUserName == null || editTextUserName.getText().toString().isEmpty() )
                || (editTextPassword == null || editTextPassword.getText().toString().isEmpty() ) ){

            Toast.makeText(this, "Username & password cannot be empty", Toast.LENGTH_SHORT).show();

        }else if (vehicle_id.contentEquals("0")){

            Toast.makeText(this, "Please go to settings and set Route Id & other fields", Toast.LENGTH_SHORT).show();
        }else {

            SharedPreferences.Editor editor = sharedPrefs.edit();
            editor.putString(constants.username, editTextUserName.getText().toString());
            editor.putString(constants.password, editTextPassword.getText().toString());
            editor.apply();
            utility.Login(this, sharedPrefs, this);
        }
    }


    public void MoveToRouteMaster(){
        Intent routemasterIntent = new Intent(getApplicationContext(), activityroutemaster.class);
        routemasterIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(routemasterIntent);
        finish();
    }



    /* Checks if external storage is available for read and write */
    public static boolean isExternalStorageWritable() {
        String state = Environment.getExternalStorageState();
        if ( Environment.MEDIA_MOUNTED.equals( state ) ) {
            return true;
        }
        return false;
    }

    /* Checks if external storage is available to at least read */
    public static boolean isExternalStorageReadable() {
        String state = Environment.getExternalStorageState();
        if ( Environment.MEDIA_MOUNTED.equals( state ) ||
                Environment.MEDIA_MOUNTED_READ_ONLY.equals( state ) ) {
            return true;
        }
        return false;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        setLogo();
    }

    public void setLogo(){

        SharedPreferences sharedPrefs = PreferenceManager.getDefaultSharedPreferences(this);
        String logo_selected = sharedPrefs.getString(constants.logo_check, "CodeLand");
        ImageView imageView = (ImageView)findViewById(R.id.splash_logo);

        switch (logo_selected){
            case constants.CodeLand:
                imageView.setImageResource(R.drawable.codeland_logo);
                break;
            case constants.Medicare:
                imageView.setImageResource(R.drawable.medicare_logo);
                break;
            case constants.Maridi:
                imageView.setImageResource(R.drawable.maridi_logo);
                break;
            case constants.AECS:
                imageView.setImageResource(R.drawable.codeland_logo);
                break;
            default:
                imageView.setImageResource(R.drawable.codeland_logo);
                break;

        }
    }
}

