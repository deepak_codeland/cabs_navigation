package com.cabs.softwarescanner.ui;

import android.widget.EditText;
import android.widget.ImageView;

import org.json.JSONArray;

import java.util.HashMap;
import java.util.Stack;

public class Bags {
    private EditText data_count = null;
    private EditText data_weight = null;
    private ImageView minus;
    private ImageView plus;
    private Boolean canDecrement = false;

    public int getManuallyEnteredCount() {
        return manuallyEnteredCount;
    }

    public double getManuallyEnteredWeight() {
        return manuallyEnteredWeight;
    }

    private int manuallyEnteredCount = 0;
    private double manuallyEnteredWeight = 0.0;

    int bag_count = 0;
    double bag_weight = 0.0;
    int category_id =0;
    JSONArray barcodes = new JSONArray();
    private HashMap<String, Double> bag_details = new HashMap<>();
    private Stack<Double> manual_bag_details = new Stack<>();

    Bags(EditText count, EditText weight, int category_id, ImageView minus, ImageView plus){
        this.data_count = count;
        this.data_weight = weight;
        this.category_id = category_id;
        this.minus = minus;
        this.plus = plus;
    }

    public Bags() {

    }

    void addBarcode(String barcode) {
        String[] tokens_unclean = barcode.split(":");
        bag_details.put(tokens_unclean[0], Double.parseDouble(tokens_unclean[1]));
    }

    void addManualBagDetails(double weight){
        manual_bag_details.push(weight);
    }

    double removeManualBagDetails() {
        if (!manual_bag_details.empty()) {
            return manual_bag_details.pop();
        }else {
            return 0.0;
        }
    }

    void updateData() {
        try {
            bag_count = Integer.parseInt(data_count.getText().toString());
        } catch (Exception e) {
            bag_count = 0;
        }

        try {
            bag_weight = Double.parseDouble(data_weight.getText().toString());
        } catch (Exception e) {
            bag_weight = 0.0;
        }
    }

    void showCount() {
        bag_weight = 0.0;
        double manualWeight = 0.0;
        if(bag_details.size()> 0) {
            for (HashMap.Entry<String, Double> entry : bag_details.entrySet()) {
                bag_weight += entry.getValue();
                barcodes.put(entry.getKey());
            }
            bag_count = bag_details.size();
        }

        if (!manual_bag_details.empty()){
            for (int i = 0; i < manual_bag_details.size(); i++){
                manualWeight = manualWeight + manual_bag_details.elementAt(i);
            }
        }else {
            manualWeight = 0.0;
        }
        int manualCount = manual_bag_details.size();
        int totalCount = bag_count + manualCount + manuallyEnteredCount;
        String cnt = Integer.toString(totalCount);
        data_count.setText(cnt);

        double totalWeight = bag_weight + manualWeight + manuallyEnteredWeight;
        totalWeight = totalWeight*100;
        totalWeight = Math.round(totalWeight);
        totalWeight = totalWeight /100;
        String wt = Double.toString(totalWeight);
        data_weight.setText(wt);
    }

    EditText getData_count() {
        return data_count;
    }

    EditText getData_weight() {
        return data_weight;
    }

    //having data to be stored which was added manually(through keyboard)
    public int setManuallyEnteredCount(int manuallyEnteredCount) {
        this.manuallyEnteredCount = manuallyEnteredCount;
        return this.manuallyEnteredCount;
    }

    public double setManuallyEnteredWeight(double manuallyEnteredWeight) {
        this.manuallyEnteredWeight = manuallyEnteredWeight;
        return this.manuallyEnteredWeight;
    }

}
