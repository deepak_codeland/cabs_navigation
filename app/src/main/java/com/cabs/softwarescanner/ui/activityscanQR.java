package com.cabs.softwarescanner.ui;

import android.Manifest;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.hardware.Camera;
import android.media.AudioManager;
import android.media.ToneGenerator;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.os.Vibrator;
import android.preference.PreferenceManager;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.cabs.softwarescanner.R;
import com.cabs.softwarescanner.constants;
import com.dynamsoft.barcode.Barcode;
import com.dynamsoft.barcode.BarcodeReader;
import com.dynamsoft.barcode.FinishCallback;
import com.dynamsoft.barcode.ReadResult;

import java.util.List;
import java.util.concurrent.locks.ReentrantReadWriteLock;

import static com.cabs.softwarescanner.ui.activityscan.PERMISSIONS_REQUEST_CAMERA;

public class activityscanQR extends AppCompatActivity implements Camera.PreviewCallback{

    public static String TAG = "DBRDemo";
    private String contents = "";

    private FrameLayout mPreview = null;
    private BarcodeReader mBarcodeReader;
    private long mBarcodeFormat = Barcode.QR_CODE;
    private ImageView mFlashImageView;
    private TextView mFlashTextView;
    private RectLayer mRectLayer;
    private CameraPreview mSurfaceHolder = null;
    private Camera mCamera = null;
    private boolean mIsDialogShowing = false;
    private boolean mIsReleasing = false;
    final ReentrantReadWriteLock mRWLock = new ReentrantReadWriteLock();
    private static boolean isCameraReleased = true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_scanqr);

        setLogo();
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        contents ="";
        //apply for camera permission
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA) !=  PackageManager.PERMISSION_GRANTED) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                    Manifest.permission.CAMERA)) {
            }
            else {
                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.CAMERA}, PERMISSIONS_REQUEST_CAMERA);
            }
        }

        mPreview = (FrameLayout) findViewById(R.id.camera_previewQR);
        //SDK Key for dynamsoft barcode reader
        mBarcodeReader = new BarcodeReader(constants.BARCODE_LICENSE);
        mFlashImageView = (ImageView)findViewById(R.id.ivFlash);
        mFlashTextView = (TextView)findViewById(R.id.tvFlash);
        mRectLayer = (RectLayer)findViewById(R.id.rectLayer);

        mSurfaceHolder = new CameraPreview(activityscanQR.this);
        mPreview.addView(mSurfaceHolder);
    }

    @Override
    protected void onResume() {
        super.onResume();
        waitForRelease();
        if (mCamera == null) {
            openCamera();
        }else {
            mCamera.startPreview();
            mCamera.setPreviewCallback(activityscanQR.this);
        }

    }

    @Override
    protected void onPause() {
        super.onPause();
        if (mCamera != null) {
            mCamera.stopPreview();
            mCamera.setPreviewCallback(null);
            mSurfaceHolder.getHolder().removeCallback(mSurfaceHolder);
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (mCamera != null) {
            mSurfaceHolder.stopPreview();
            mCamera.setPreviewCallback(null);
            mSurfaceHolder.getHolder().removeCallback(mSurfaceHolder);
            mIsReleasing = true;
            releaseCamera();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        waitForRelease();
        if (mCamera != null) {
            mCamera.stopPreview();
            mCamera.setPreviewCallback(null);
            mSurfaceHolder.getHolder().removeCallback(mSurfaceHolder);
            mCamera.release();
            mCamera = null;
        }
    }


    @Override
    public void finish() {
        super.finish();
        Intent intent=new Intent();
        intent.putExtra("RESULT_STRING", contents);
        setResult(RESULT_OK, intent);
    }

    public void setComplete() {
        Intent intent=new Intent();
        intent.putExtra("RESULT_STRING", contents);
        setResult(RESULT_OK, intent);
        finish();
    }

    public void setFlash(View v) {
        if (mCamera != null) {

            try {
                Camera.Parameters p = mCamera.getParameters();
                String flashMode = p.getFlashMode();
                if (flashMode.equals(Camera.Parameters.FLASH_MODE_OFF)) {
                    p.setFlashMode(Camera.Parameters.FLASH_MODE_TORCH);
                    mFlashImageView.setImageResource(R.mipmap.flash_on);
                } else {
                    p.setFlashMode(Camera.Parameters.FLASH_MODE_OFF);
                    mFlashImageView.setImageResource(R.mipmap.flash_off);
                }
                mCamera.setParameters(p);
                mCamera.startPreview();
            }catch (Exception e){
                Toast.makeText(this, "Flash not available!", Toast.LENGTH_SHORT).show();
            }
        }
    }


    private void releaseCamera()
    {
        new Thread(new Runnable() {
            @Override
            public void run() {
                mCamera.release();
                mCamera = null;
                mRWLock.writeLock().lock();
                mIsReleasing = false;
                mRWLock.writeLock().unlock();
            }
        }).start();
    }

    private void waitForRelease() {
        while (true) {
            mRWLock.readLock().lock();
            if (mIsReleasing) {
                mRWLock.readLock().unlock();
                try {
                    Thread.sleep(10);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            } else {
                mRWLock.readLock().unlock();
                break;
            }
        }
    }

    private boolean mFinished = true;
    private final static int READ_RESULT = 1;
    private final static int OPEN_CAMERA = 2;
    private final static int RELEASE_CAMERA = 3;
    Handler handler = new Handler(Looper.getMainLooper()) {
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case READ_RESULT:

                    ReadResult result = (ReadResult)msg.obj;
                    Barcode barcode = result.barcodes == null ? null : result.barcodes[0];
                    if (barcode != null) {

                        contents = result.barcodes[0].displayValue + "";
                        ToneGenerator toneGen1 = new ToneGenerator(AudioManager.STREAM_MUSIC, 100);
                        toneGen1.startTone(ToneGenerator.TONE_CDMA_PIP, 150);
                        ((Vibrator) getSystemService(VIBRATOR_SERVICE)).vibrate(250);
                        setComplete();
                    } else {
                        if (result.errorCode != BarcodeReader.DBR_OK)
                            Log.i(TAG, "Error:" + result.errorString);
                    }
                    mFinished = true;

                    break;
                case OPEN_CAMERA:
                    if (mCamera != null) {
                        mCamera.setPreviewCallback(activityscanQR.this);
                        mSurfaceHolder.setCamera(mCamera);
                        Camera.Parameters p = mCamera.getParameters();
                        if (mFlashTextView.getText().equals("Flash on"))
                            p.setFlashMode(Camera.Parameters.FLASH_MODE_TORCH);
                        mCamera.setParameters(p);
                        mSurfaceHolder.startPreview();
                    }
                    break;
                case RELEASE_CAMERA:

                    break;
            }
        }
    };

    private static Camera getCameraInstance(){
        Camera c = null;
        try {
            c = Camera.open(); // attempt to get a Camera instance
        }
        catch (Exception e){
            Log.i(TAG, "Camera is not available (in use or does not exist)");
        }
        return c; // returns null if camera is unavailable
    }

    private void openCamera()
    {
        new Thread(new Runnable() {
            @Override
            public void run() {
                mCamera = getCameraInstance();
                if (mCamera != null) {

                    mCamera.setPreviewCallback(activityscanQR.this);
                    mCamera.setDisplayOrientation(90);
                    Camera.Parameters cameraParameters = mCamera.getParameters();

                    List<String> supportedFocusModes = cameraParameters.getSupportedFocusModes();

                    if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.ICE_CREAM_SANDWICH &&
                            supportedFocusModes.contains(Camera.Parameters.FOCUS_MODE_CONTINUOUS_VIDEO)) {
                        cameraParameters.setFocusMode(Camera.Parameters.FOCUS_MODE_CONTINUOUS_VIDEO);
                    }
                    else if (supportedFocusModes.contains(Camera.Parameters.FOCUS_MODE_CONTINUOUS_PICTURE)) {
                        cameraParameters.setFocusMode(Camera.Parameters.FOCUS_MODE_CONTINUOUS_PICTURE);
                    }
                    else if (supportedFocusModes.contains(Camera.Parameters.FOCUS_MODE_MACRO)) {
                        cameraParameters.setFocusMode(Camera.Parameters.FOCUS_MODE_MACRO);
                    }
                    else if (supportedFocusModes.contains(Camera.Parameters.FOCUS_MODE_AUTO)) {
                        // auto focus on request only
                        cameraParameters.setFocusMode(Camera.Parameters.FOCUS_MODE_AUTO);
                    }
                    mCamera.setParameters(cameraParameters);
                }


                Message message = handler.obtainMessage(OPEN_CAMERA, 1);
                message.sendToTarget();
            }
        }).start();
    }

    @Override
    public void onPreviewFrame(byte[] data, Camera camera) {
        if (mFinished && !mIsDialogShowing) {
            mFinished = false;
            Camera.Size size = camera.getParameters().getPreviewSize();
            mBarcodeReader.readSingleAsync(data, size.width, size.height, mBarcodeFormat, new FinishCallback() {
                @Override
                public void onFinish(ReadResult readResult) {
                    Message message = handler.obtainMessage(READ_RESULT, readResult);
                    message.sendToTarget();
                }
            });
        }
    }

    public void setLogo(){

        SharedPreferences sharedPrefs = PreferenceManager.getDefaultSharedPreferences(this);
        String logo_selected = sharedPrefs.getString(constants.logo_check, "CodeLand");
        ImageView imageView = (ImageView)findViewById(R.id.imageViewBack);

        switch (logo_selected){
            case constants.CodeLand:
                imageView.setImageResource(R.drawable.back_codeland);
                break;
            case constants.Medicare:
                imageView.setImageResource(R.drawable.back_medicare);
                break;
            case constants.Maridi:
                imageView.setImageResource(R.drawable.back_maridi);
                break;
            case constants.AECS:
                imageView.setImageResource(R.drawable.back_codeland);
                break;
            default:
                imageView.setImageResource(R.drawable.back_codeland);
                break;

        }
    }


}
