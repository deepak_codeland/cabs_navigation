package com.cabs.softwarescanner.service;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.os.IBinder;
import android.preference.PreferenceManager;
import android.util.Log;

import com.cabs.softwarescanner.constants;
import com.cabs.softwarescanner.database.customerdb;
import com.cabs.softwarescanner.interfaces.IAsyncResponse;
import com.cabs.softwarescanner.utils.utility;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

public class datasyncerservice extends Service {

    private boolean mRunThread = true;
    Context mcontext;

    @Override
    public void onCreate() {
        mRunThread = true;
        mcontext = this;
        new Thread(new datasyncer()).start();
        super.onCreate();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        return START_STICKY;
    }

    @Override
    public void onDestroy() {
        mRunThread = false;
        super.onDestroy();
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    private final class datasyncer implements Runnable, IAsyncResponse {

        datasyncer() {
        }

        @Override
        public void run() {

            while (mRunThread) try {
                Cursor c = null;
                try {

                    SharedPreferences pref = getSharedPreferences(constants.settings_filename, MODE_PRIVATE);
                    String session_id = pref.getString(constants.session_id, "");

                    customerdb db = customerdb.getInstance(mcontext);
                    c = db.getCustomerstoSync();

                    while (c.moveToNext()) {
                        HttpURLConnection connection;
                        URL posturl = new URL(constants.COLLECTION_URL);
                        connection = (HttpURLConnection) posturl.openConnection();
                        connection.setRequestMethod("POST");
                        connection.setRequestProperty("Content-Type", "application/json");
                        connection.setRequestProperty("X-Openerp-Session-Id", session_id);
                        connection.setDoInput(true);
                        connection.setDoOutput(true);

                        //Send request
                        DataOutputStream wr = new DataOutputStream(connection.getOutputStream());
                        wr.write(c.getString(2).getBytes("UTF-8"));
                        wr.flush();
                        wr.close();

                        InputStream is = connection.getInputStream();
                        BufferedReader rd = new BufferedReader(new InputStreamReader(is));
                        String line;
                        StringBuilder response = new StringBuilder();
                        while ((line = rd.readLine()) != null) {
                            response.append(line);
                            response.append('\r');
                        }
                        rd.close();
                        onPostExecute(new JSONObject(response.toString()), c.getInt(0));
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (Exception ex) {
                    ex.printStackTrace();
                } finally {
                    if (c != null)
                        c.close();
                }
                Thread.sleep(120000);
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }

        void onPostExecute(JSONObject result, int id) {

            JSONObject expiry = result.optJSONObject("error");
            if(expiry !=null) {
                String response_message = expiry.optString("message", "Active");
                if (response_message.equals("Odoo Session Expired")) {
                    SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
                    utility.Login(getApplicationContext(), pref, this);
                    return;
                }
                return;
            }

            String response = result.optString("result", "error");
            if( response.equals("success")) {
                customerdb.getInstance(mcontext).setSynced(id,true,true);
                return;
            }
        }

        @Override
        public void processFinish(JSONObject jsonObject) {

            try {
                SharedPreferences pref = getSharedPreferences(constants.settings_filename, MODE_PRIVATE);
                JSONObject json = jsonObject.optJSONObject("result");
                utility.saveSession(pref, json.getString(constants.session_id), json.optLong(constants.session_expiry));

            } catch (JSONException e) {
                e.printStackTrace();
                Log.v(mTag, "Bad json response");
            } catch (NullPointerException e) {
                e.printStackTrace();
                Log.v(mTag, "Failed to login");
            }
        }
        private String mTag = "Data Syncer";
    }
}
