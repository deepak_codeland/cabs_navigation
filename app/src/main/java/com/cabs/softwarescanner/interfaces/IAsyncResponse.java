package com.cabs.softwarescanner.interfaces;

import org.json.JSONObject;

public interface IAsyncResponse {
   public void processFinish(JSONObject response);
}