package com.cabs.softwarescanner;

/**
 * Created by admin on 1/11/2017.
 */

public class BagDetails {

    String CustomerName ;
    int[] bagCounts;
    double[] bagWeights;

    public BagDetails(String customerName, int[] bagCounts, double[] bagWeights) {
        CustomerName = customerName;
        this.bagCounts = bagCounts;
        this.bagWeights = bagWeights;
    }

    public String getCustomerName() {
        return CustomerName;
    }

    public int[] getBagCounts() {
        return bagCounts;
    }

    public double[] getBagWeights() {
        return bagWeights;
    }
}
