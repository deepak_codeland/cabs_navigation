Android Key: AIzaSyCA3vBNuGKHBUmW5WLacHC0YDUhWCjmgZ0
keytool -list -v -keystore "<path>\androidAppKeys.jks" -alias codeland.in -storepass codeland2015 -keypass codeland2015


Alias name: codeland.in
Creation date: Apr 8, 2016
Entry type: PrivateKeyEntry
Certificate chain length: 1
Certificate[1]:
Owner: CN=CodeLand InfoSolutions Pvt Ltd, OU=Bio-Medical Waste Management, O=CodeLand InfoSolutions Pvt Ltd, L=Bangalore, ST=Karnataka, C=India
Issuer: CN=CodeLand InfoSolutions Pvt Ltd, OU=Bio-Medical Waste Management, O=CodeLand InfoSolutions Pvt Ltd, L=Bangalore, ST=Karnataka, C=India
Serial number: f02e0d9
Valid from: Fri Apr 08 14:58:41 IST 2016 until: Tue Apr 02 14:58:41 IST 2041
Certificate fingerprints:
         MD5:  AF:D1:6F:97:D0:4D:F2:C7:7F:A5:E2:2B:88:22:3C:89
         SHA1: AD:D7:45:20:14:51:58:8A:59:74:B4:B7:1E:85:30:8D:AB:76:EA:8E
         SHA256: BD:39:F7:51:9C:13:68:D2:52:92:60:FF:0A:6F:3B:69:02:3F:BB:BC:2D:0A:F6:33:64:AE:11:8B:24:39:73:AA
         Signature algorithm name: SHA256withRSA
         Version: 3

Extensions:

#1: ObjectId: 2.5.29.14 Criticality=false
SubjectKeyIdentifier [
KeyIdentifier [
0000: 55 EC 47 AB D3 DE 69 85   56 01 FC D1 00 0A DA 1A  U.G...i.V.......
0010: 64 BD 88 74                                        d..t
]